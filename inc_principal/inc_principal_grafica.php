<div class="span12">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Criterios / Puntos(%)</h5>
    </div>
    <div class="widget-content ">
      <div id="container_line" style="min-width: 100%; height: 100%; max-width: 600px; margin: 0 auto"></div>
    </div>
  </div>
</div>

<!------------------------------- GRAFICA DE LINEAL -------------------------------->
<script type="text/javascript">
  Highcharts.chart('container_line', {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Criterios por periodo'
    },
    xAxis: {
        categories: [<?php echo $categorias_lineal; ?>]
    },
    yAxis: {
        title: {
            text: 'Puntos EFQM'
        },
        labels: {
            formatter: function () {
                return this.value+'%';
            }
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    },
    series: [<?php 
        for ($i=0; $i < count($series); $i++) { ?>
            {
                name: <?php echo $series[$i] ?>,
                marker: {
                symbol: 'square'
                },
                data: [<?php echo $data_lineal[$i]; ?>]
            },           
        <?php }?>
    ]
});
</script>
<!------------------------------- // FIN GRAFICA DE LINEAL -------------------------------->