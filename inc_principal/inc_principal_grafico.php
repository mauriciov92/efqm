<div class="row-fluid">
  <div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
      <h5>Ultimos dos periodos</h5>
    </div>
    <div class="widget-content" >
      <div class="row-fluid">
        <div class="span12">
          <div class="chart"></div>
        </div>
      </div>
    </div>
  </div>
</div>