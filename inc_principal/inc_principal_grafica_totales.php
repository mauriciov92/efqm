<div class="span12">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Puntos EFQM - Periodos</h5>
    </div>
    <div class="widget-content">
      <div id="container_barra" style="min-width: 100%; height: 100%; max-width: 600px; margin: 0 auto"></div>
    </div>
  </div>
</div>

<!------------------------------- GRAFICA DE LINEAL -------------------------------->
<script type="text/javascript">
// Create the chart
Highcharts.chart('container_barra', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Comparación de periodos'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Puntos EFQM'
        },
        min:0,
        max:1000,

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> de 1000 puntos<br/>'
    },

    series: [{
        name: 'Periodo',
        colorByPoint: true,
        data: [<?php echo $series_barra; ?>]
    }],
});
</script>
<!------------------------------- // FIN GRAFICA DE LINEAL -------------------------------->