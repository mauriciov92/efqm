<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 

/* DEFINICION DE VARIABLES*/

  $idperiodo=$_GET['idperiodo'];

/*// FIN DEFINICION DE VARIABLES*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Mejoras</a></div>
    </div>
    <div class="container-fluid">     
      <div class="row-fluid">
        <?php include "inc_mejoras/inc_mejoras_edicion_query.php" ?>
        <?php include "inc_mejoras/inc_mejoras_edicion.php" ?>
      </div>        
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function() {
  $('#menu_principal').removeAttr('class');
  $('#menu_areas_mejora').attr('class', 'submenu active');
});
</script>
    
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>