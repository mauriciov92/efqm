<?php 
  $tipo_persona=$_SESSION['tipo_persona'];
?>
<div id="sidebar" class="hidden-print"><a href="#" class="visible-phone"><i class="fa fa-bars"></i> Men&uacute;</a>
  <ul>
    <li id="menu_principal" class="active"><a href="principal.php"><i class="icon icon-home"></i> <span>Principal</span></a> </li>
    <li id="menu_periodo" class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Periodos</span><?php if ($tipo_persona!=4 && $tipo_persona!=3) { ?> <span class="label label-important">2</span><?php } ?></a>
      <ul>
        <li><a href="periodo_principal.php">Periodos</a></li>
        <?php if ($tipo_persona!=4 && $tipo_persona!=3) { ?>
          <li><a href="periodo_nuevo.php">Nuevo periodo</a></li>
        <?php } ?>
      </ul>
    </li>
    <li id="menu_encuesta" class="submenu"> <a href="#"><i class="fa fa-file-text"></i> <span>Encuestas</span><?php if ($tipo_persona!=4 && $tipo_persona!=3) { ?> <span class="label label-important">2</span> <?php } ?></a>
      <ul>
        <li><a href="encuestas_principal.php">Encuestas</a></li>
        <?php if ($tipo_persona!=4 && $tipo_persona!=3) { ?>
        <li><a href="encuenta_alta.php">Nueva Encuesta</a></li>
        <?php }?>
      </ul>
    </li>
    <!--
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Criterios</span> <span class="label label-important">3</span></a>
      <ul>
        <li><a href="#">Criterios</a></li>
        <li><a href="#">Subcriterio</a></li>
      </ul>
    </li>
  -->
    <li id="menu_evidencias"> <a href="evidencias_principal.php"><i class="fa fa-book"></i> <span>Evidencias</span></a> </li>
    <li id="menu_areas_mejora"> <a href="mejoras_principal.php"><i class="fa fa-stack-overflow" aria-hidden="true"></i> <span>Areas de mejora</span></a> </li>
    <li id="menu_info_efqm"> <a href="info_efqm.php"><i class="fa fa-info-circle" aria-hidden="true"></i> <span>¿Que es EFQM?</span></a> </li>
    <li id="menu_manuales"> <a href="manuales.php"><i class="fa fa-book" aria-hidden="true"></i> <span>Manuales</span></a> </li>
    <?php 
      mysql_select_db($database_conexion_efqm, $conexion_efqm);

      $periodo_menu=
      "SELECT 
          *
      FROM
          periodo
      WHERE
          periodo_anio = (SELECT 
                  MAX(periodo_anio)
              FROM
                  periodo)";
      $q_periodo_menu=mysql_query($periodo_menu) or die(mysql_error());
      $row_periodo_menu=mysql_fetch_array($q_periodo_menu);
      $id_ultimo_periodo=$row_periodo_menu['idperiodo'];

      $puntos_periodo=
      "SELECT 
          SUM(ptos_totales_obtenidos) AS puntos_periodo
      FROM
          periodo_has_criterio
              INNER JOIN
          periodo ON periodo_idperiodo = idperiodo
      WHERE
          idperiodo = $id_ultimo_periodo";
      $q_puntos_periodo=mysql_query($puntos_periodo) or die(mysql_error());
      $row_puntos_periodo=mysql_fetch_array($q_puntos_periodo);
      $puntos=$row_puntos_periodo['puntos_periodo'];      
      $porcentaje_barra=$puntos*100/1000;
      ?>
    <li class="content"> <span>Puntos &Uacute;ltimo Periodo (<?php echo $row_periodo_menu['periodo_anio']; ?>)</span>
      <div class="progress progress-mini progress-danger active progress-striped">
        <div style="width: <?php echo $porcentaje_barra;?>%; ?>" class="bar"></div>
      </div>
      <span class="percent"><?php echo round($puntos,2); ?></span>
      <div class="stat"><?php echo round($puntos,2); ?> / 1000 Ptos</div>
    </li>    
    <li>
      <?php 
        switch (true) {
          case $puntos<300: ?>
            <div align="center"><img src="img/compromiso.png" width="40%" class="img-responsive" alt="Image"></div>
            <?php 
            break;
          
          case $puntos>=300 && $puntos<400: ?>
            <div align="center"><img src="img/mas_300.png" width="40%" class="img-responsive" alt="Image"></div>
            <?php 
            break;

          case $puntos>=400 && $puntos<500: ?>
            <div align="center"><img src="img/mas_400.png" width="40%" class="img-responsive" alt="Image"></div>
            <?php 
            break;

          case $puntos>=500: ?>
            <div align="center"><img src="img/mas_500.png" width="40%" class="img-responsive" alt="Image"></div>
            <?php 
            break;
          default:
            break;
        }
      ?>
    </li>
  </ul>
</div>