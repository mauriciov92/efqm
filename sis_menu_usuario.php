<div id="header">
  <h1 class="hidden-phone hidden-tablet"><a href="#">EFQM</a></h1>
</div>
<div id="user-nav" class="navbar navbar-inverse hidden-print">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Bienvenido <?php echo $_SESSION["usuarioactual"]; ?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i> Perfil</a></li>
        <li class="divider"></li>
        <li><a href="sis_logout.php"><i class="icon-key"></i> Salir</a></li>
      </ul>
    </li>
    <!--
    <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Mensajes</span> <span class="label label-important">5</span> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> Nuevo Mensaje</a></li>
        <li class="divider"></li>
        <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
      </ul>
    </li>
    -->
    <!--
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Configuraci&oacute;n</span></a></li>
  -->
    <li class=""><a title="" href="sis_logout.php"><i class="icon icon-share-alt"></i> <span class="text">Salir</span></a></li>
  </ul>
</div>
<div id="search">
  <input type="text" placeholder="Buscar aqu&iacute;..."/>
  <button type="submit" class="tip-bottom" title="Buscar"><i class="icon-search icon-white"></i></button>
</div>