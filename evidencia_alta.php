<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
/* DEFINICION DE VARIABLES*/

$idperiodo=$_GET['idperiodo'];

/// FIN DEFINICION DE VARIABLES/
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <?php include "sis_header.php"; ?>
 
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">    
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span12">
              <?php include "inc_evidencias/inc_evidencia_query.php" ?>
              <?php include "inc_evidencias/inc_evidencia_alta.php" ?>

          </div>
        </div>
      </div>
    </div>
    
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?> 


</body>
</html>