<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Periodos</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Id Periodo</th>
                  <th>A&ntilde;o</th>
                  <th>Puntos EFQM</th>
                  <th>Areas encuestadas</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                while ($row_periodo=mysql_fetch_array($q_periodo)) {
                  $comprobacion_periodo=0;
                  $estado_periodo=$row_periodo['periodo_estado'];
                  $estado_encuestas='';
                  $calse_periodo='';
                  /* CALCULO DE PUNTOS EFQM DEL PERIODO */  
                  $puntos_totales=
                  "SELECT 
                      SUM(ptos_totales_obtenidos) AS efqm_obtenido
                  FROM
                      periodo_has_criterio
                  WHERE
                      periodo_idperiodo = $row_periodo[idperiodo]";
                  $q_puntos_totales=mysql_query($puntos_totales) or die(mysql_error());
                  $row_puntos_totales=mysql_fetch_array($q_puntos_totales);
                  $puntos_efqm_periodo=$row_puntos_totales['efqm_obtenido'];
                  /* FIN CALCULO DE PUNTOS EFQM DEL PERIODO */

                  /* VERIFICACION DE ESTADOS DE LAS ENCUESTAS DEL PERIODO */
                  $encuestas_periodo=
                  "SELECT 
                      idencuesta, encuesta_estado
                  FROM
                      encuesta_calculo
                          INNER JOIN
                      encuesta ON encuesta_idencuesta = idencuesta
                  WHERE
                      periodo_idperiodo = $row_periodo[idperiodo]
                  GROUP BY encuesta_idencuesta";
                  $q_encuestas_periodo=mysql_query($encuestas_periodo) or die(mysql_error());
                  $cantidad_encuestas=mysql_num_rows($q_encuestas_periodo);
                  while ($row_encuestas_periodo=mysql_fetch_array($q_encuestas_periodo)) {
                    if ($row_encuestas_periodo['encuesta_estado'] != 'Finalizada') {
                      $comprobacion_periodo=1;
                    }
                  }
                  if ($comprobacion_periodo==1) { 
                    $calse_periodo='class="warning"';
                    $estado_encuestas="Encuestas incompletas (Calculo de puntos parcial).";
                  }
                  else{ 
                    $calse_periodo='class="success"';
                    $estado_encuestas='Encuestas completas.'; 
                  }
                  ?>
                  <tr style="text-align: center;" <?php echo $calse_periodo; ?> >
                    <td><a href="periodo_ver.php?idperiodo=<?php echo $row_periodo['idperiodo']; ?>"><?php echo $row_periodo['idperiodo']; ?></a></td>
                    <td><a href="periodo_ver.php?idperiodo=<?php echo $row_periodo['idperiodo']; ?>"><?php echo $row_periodo['periodo_anio']; ?></a></td>
                    <td><a href="periodo_ver.php?idperiodo=<?php echo $row_periodo['idperiodo']; ?>"><?php echo round($puntos_efqm_periodo,2); ?></a></td>
                    <td><a href="periodo_ver.php?idperiodo=<?php echo $row_periodo['idperiodo']; ?>"><?php echo $cantidad_encuestas; ?></a></td>
                    <td><a href="periodo_ver.php?idperiodo=<?php echo $row_periodo['idperiodo']; ?>"><?php echo $estado_encuestas.' - Periodo: '.$estado_periodo; ?></a></td>
                  </tr>
                <?php } ?>                
              </tbody>
            </table>
          </div>
        </div>
      </div>      
    </div>
  </div>
