<div class="span12 hidden-print">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Evidencias</h5>
    </div>
    <div class="widget-content">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Evidencia</th>
            <th>Descripci&oacute;n</th>
            <th>Areas relacionadas</th>
            <th>Subcriterios relacionados</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if (mysql_num_rows($q_evidencia_periodo) >0) {
          while ($row_evidencia_periodo=mysql_fetch_array($q_evidencia_periodo)) { 

            $area_evidencia=
            "SELECT 
                area_nombre
            FROM
                subcriterio_has_evidencia
                    INNER JOIN
                area ON area_idarea = idarea
            WHERE
                periodo_idperiodo = $idperiodo
                    AND evidencia_idevidencia = $row_evidencia_periodo[idevidencia]
            GROUP BY idarea";
            $q_area_evidencia=mysql_query($area_evidencia) or die(mysql_error());

            $subcriterio_evidencia=
            "SELECT 
                subcriterio_nombre
            FROM
                subcriterio_has_evidencia
                    INNER JOIN
                subcriterio ON subcriterio_idsubcriterio = idsubcriterio
            WHERE
                periodo_idperiodo = $idperiodo
                    AND evidencia_idevidencia = $row_evidencia_periodo[idevidencia]
            GROUP BY idsubcriterio";
            $q_subcriterio_evidencia=mysql_query($subcriterio_evidencia) or die(mysql_error());
            ?>
            <tr>
              <td>
                <a href="<?php echo $row_evidencia_periodo['evidencia_url']; ?>" download><?php echo $row_evidencia_periodo['evidencia_url']; ?></a>                  
              </td> 
              <td><?php echo $row_evidencia_periodo['evidencia_descripcion']; ?></td>                
              <td>
                <?php   
                while ($row_area_evidencia=mysql_fetch_array($q_area_evidencia)) {
                  echo $row_area_evidencia['area_nombre'].', ';
                }
                ?>
              </td>
              <td>
                <?php 
                while ($row_subcriterio_evidencia=mysql_fetch_array($q_subcriterio_evidencia)) {
                  echo $row_subcriterio_evidencia['subcriterio_nombre'].', ';
                }
                ?>
              </td>
          	</tr>  
          <?php } 
          }else{ ?>
          	<tr>
              <td colspan="4">
              	<div class="alert alert-info" align="center">
              		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              		<strong>No hay evidencias cargadas para este periodo</strong>
              	</div>
              </td> 
          	</tr>
          <?php } ?>
          <?php if ($tipo_persona!=4 && $tipo_persona!=3) { ?>
          <tr>
            <td colspan="4">
              <div align="right">
                <a class="btn btn-link" href='evidencia_alta.php?idperiodo=<?php echo $idperiodo;?>'>Evidencias <i class="fa fa-2x fa-plus-circle"></i></a>  
              </div>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</div>