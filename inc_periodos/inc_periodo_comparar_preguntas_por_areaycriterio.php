<div class="row-fluid">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>Informe Comparaci&oacute;n Criterios por Areas y Resultados de Preguntas <?php echo $anio_periodo1; ?> - <?php echo $anio_periodo2; ?></h5>
            </div>
            <div class="widget-content nopadding">
                
                <div style="width: 45%!important; display: inline-block;  margin-left: 30px;">
                    <select id="selectArea" class="form-control" required="required">
                        <option value="0">Seleccione Area</option>
                        <?php
                        $consulta_areas =
                        "
                        SELECT  a.idarea, a.area_nombre
                        FROM area a
                        INNER JOIN  encuesta e ON a.idarea = e.area_idarea
                        INNER JOIN encuesta_calculo ec ON e.idencuesta = ec.encuesta_idencuesta
                        INNER JOIN
                            (SELECT a2.idarea, a2.area_nombre, ec2.periodo_idperiodo
                                FROM area a2
                                INNER JOIN encuesta e2 ON a2.idarea = e2.area_idarea
                                INNER JOIN encuesta_calculo ec2 ON e2.idencuesta = ec2.encuesta_idencuesta) s2 
                        ON s2.idarea = a.idarea
                        WHERE
                        ec.periodo_idperiodo = $idperiodo1
                        AND s2.periodo_idperiodo = $idperiodo2
                        GROUP BY a.idarea
                        ";
                        $q_consulta_areas=mysql_query($consulta_areas) or die(mysql_error());
                        while ($row_consulta_areas = mysql_fetch_array($q_consulta_areas)) {
                            echo '<option value="'.$row_consulta_areas['idarea'].'">'.$row_consulta_areas['area_nombre'].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div style="width: 45%!important; display: inline-block;  margin-left: 30px;">
                    <select  id="selectCriterio" class="form-control" required="required">
                        <option value="0">Seleccione Criterio</option>
                        <option value="1">Liderazgo</option>
                        <option value="2">Politica y Estrategia</option>
                        <option value="3">Personas</option>
                        <option value="4">Alianzas y Recursos</option>
                        <option value="5">Procesos</option>
                        <option value="6">Resultados en los Clientes</option>
                        <option value="7">Resultados en las Personas</option>
                        <option value="8">Resultados en la Sociedad</option>
                        <option value="9">Resultados Claves</option>
                    </select>
                </div>
                <br>
                <br>
                <button type="submit" id="compararAreas" class="btn btn-primary" style="margin-left: 30px;">Comparar</button>
                <br>
                <br>
                <div class="widget-content nopadding" id="tablaCompararPreguntas" hidden="hidden">
                <table class="table table-bordered table-striped">
                    <thead>
                    <th>Pregunta</th>
                    <th>Resultado <?php echo $anio_periodo1?></th>
                    <th>Respuesta Pregunta</th>
                    <th>Evidencia(%)</th>
                    <th>Resultado <?php echo $anio_periodo2?></th>
                    <th>Respuesta Pregunta</th>
                    <th>Evidencia(%)</th>
                    </thead>
                    <tbody class="compararPreguntas">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {
    $(document).ready(function () {
        $("#compararAreas").on('click',function () {
            var idperiodo1 = "<?php echo $idperiodo1; ?>";
            var idperiodo2 = "<?php echo $idperiodo2; ?>";
            var criterioSeleccionado = $("#selectCriterio").children("option:selected").val();
            var areaSeleccionada = $("#selectArea").children("option:selected").val();
            if(criterioSeleccionado == "0" || areaSeleccionada == "0"){
                alert ("Debe ingresar seleccionar alguna opcion de consulta");
                return false;
            }else{          
            $.ajax({
                type: "POST",
                url: "inc_periodos/inc_periodo_comparar_preguntas_por_areaycriterio_ajax.php",
                data: {"idperiodo1":idperiodo1,"idperiodo2":idperiodo2,"criterioSeleccionado":criterioSeleccionado,"areaSeleccionada":areaSeleccionada},
                dataType:"json"
               
            }).done(function (respuesta) {
                if(respuesta.data && respuesta.data.length >=1){
                    debugger;
                    var html = "";	
                    var subcriterio="";
                    for(var i=0;i<respuesta.data.length;i++){
                        if(subcriterio!==respuesta.data[i].subcriterio_nombre){
                                html+="<tr><td colspan=7 style= text-align:center;>"+respuesta.data[i].subcriterio_nombre+" - "+respuesta.data[i].subcriterio+"</td></tr>"
                                subcriterio=respuesta.data[i].subcriterio_nombre;
                            }
							html+="<tr data-index='"+i+"'>";
							html+="<td style= font-size:12px; align='left'>"+respuesta.data[i].pregunta_descripcion+"</td>"+
							"<td style= font-size:12px;text-align:right;>"+respuesta.data[i].resultado_periodo1+"</td>";
                            if(respuesta.data[i].respuesta_periodo1){
                                html+="<td style= font-size:12px; align='left'>"+respuesta.data[i].respuesta_periodo1+"</td>";
                            }else{
                                html+="<td style= font-size:12px; align='left'>N/R</td>";
                            }
                            html+="<td style= font-size:12px;text-align:right;>"+respuesta.data[i].evidencia_periodo1+"%</td>"+
							"<td style= font-size:12px;text-align:right;>"+respuesta.data[i].resultado_periodo2+"</td>";
                            if(respuesta.data[i].respuesta_periodo2){
                                html+="<td style= font-size:12px; align='left'>"+respuesta.data[i].respuesta_periodo2+"</td>";
                            }else{
                                html+="<td style= font-size:12px; align='left'>N/R</td>";
                            }
                            html+="<td style= font-size:12px;text-align:right;>"+respuesta.data[i].evidencia_periodo2+"% </td>"
                        
							"</tr>";
						}
                        $(".compararPreguntas").empty().append(html);
                        $("#tablaCompararPreguntas").show();

                }else{
                        alert("No Fue posible realizar la comparación de los periodos");
                }

                }) ;
            }
            });

  

    });
        })(jQuery);
        
</script>