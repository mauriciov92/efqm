<?php 
$clase_fila_encuesta='';
?>
<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Mejoras <?php echo $series[0].' - '.$series[1] ?></h5>
        </div>
        <div class="widget-content nopadding">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Descripci&oacute;n</th>
                  <th>Prioridad</th>
                  <th>Area</th>
                  <th>Estado</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                  for ($i=0; $i < count($idperiodo_array); $i++) { 
                
                    $mejoras=
                    "SELECT 
                        idmejora,mejoradescriocion, estado, area_nombre, descripcion
                    FROM
                        mejora
                            INNER JOIN
                        encuesta ON encuesta_idencuesta = idencuesta
                            INNER JOIN
                        area ON area_idarea = idarea
                            INNER JOIN
                        prioridad_mejora ON prioridad_mejora_idprioridad_mejora = idprioridad_mejora
                    WHERE
                        periodo_idperiodo = $idperiodo_array[$i]
                    ORDER BY estado";
                    $q_mejoras=mysql_query($mejoras) or die(mysql_error()); 
                    $estado_mejora =
                    "SELECT COUNT(IF(estado='en proceso',1, NULL)) 'en_proceso',
                    COUNT(IF(estado='pendiente',1, NULL)) 'pendiente',
                    COUNT(IF(estado='finalizado',1, NULL)) 'finalizado'
                    FROM mejora 
                    WHERE periodo_idperiodo = $idperiodo_array[$i]";
                    $q_estado_mejora = mysql_query($estado_mejora) or die(mysql_error()); 
                    $row_estado_mejora=mysql_fetch_array($q_estado_mejora);
                    ?>
                    <tr class="periodo_titulo" onclick="mostrar_mejoras(<?php echo $series[$i]; ?>);">
                      <td colspan="4">
                        <div>
                          <strong><?php echo $series[$i].'  Cantidad Mejoras: '.mysql_num_rows($q_mejoras).' (Pendientes: '.$row_estado_mejora['pendiente'].', En Proceso: '.$row_estado_mejora['en_proceso'].', Finalizados: '.$row_estado_mejora['finalizado'].')'; ?></strong>
                        </div>
                        <?php 
                        if (mysql_num_rows($q_mejoras)>0) { ?>
                          <div align="right"><a href="#"><i class="fa fa-caret-down" aria-hidden="true"></i></a></div>
                        <?php }
                        ?>
                      </td>
                    </tr>
                    <?php 
                    while ($row_mejoras=mysql_fetch_array($q_mejoras)) {
                      switch ($row_mejoras['estado']) {
                        case 'pendiente':
                          $clase_fila_encuesta='warning';
                          break;
                        
                        case 'en proceso':
                          $clase_fila_encuesta='info';
                          break;

                        case 'finalizado':
                          $clase_fila_encuesta='success';
                          break;

                        default:
                          break;
                      } ?>
                      <tr class="<?php echo $clase_fila_encuesta;?> <?php echo $series[$i]; ?>" style="display: none;">
                        <td><a href="#"><?php echo $row_mejoras['mejoradescriocion']; ?></a></td>
                        <td><a href="#"><?php echo $row_mejoras['descripcion']; ?></a></td>
                        <td><a href="#"><?php echo $row_mejoras['area_nombre']; ?></a></td>
                        <td><a href="#"><?php echo $row_mejoras['estado']; ?></a></td>
                      </tr>
                    <?php } 
                  } ?>               
              </tbody>
            </table>
          </div>
          <div class="clearfix"></div>
          <br>
          <div><strong>*Referencias: </strong>
            <span class="label label-warning">Pendiente</span>
            <span class="label label-info">En proceso</span>
            <span class="label label-success">Finalizada</span>
          </div>
        </div>
      </div>      
    </div>
  </div>

<script type="text/javascript">
  function mostrar_mejoras(anio){
      $('.'+anio).toggle('slow');
  }
  $(document).ready(function(){

  });
</script>  