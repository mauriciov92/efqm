<?php
$puntos_totales_efqm = 0;
$puntos_totales_efqm_obtenidos_periodo1 = 0;
$puntos_totales_efqm_obtenidos_periodo2 = 0;
$porcentaje_efqm_periodo1 = 0;
$porcentaje_efqm_periodo2 = 0;
?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Informe Comparaci&oacute;n de Per&iacute;odos <?php echo $anio_periodo1; ?> - <?php echo $anio_periodo2; ?></h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Criterio</th>
              <th>Puntos Max. EFQM<?php echo $anio_periodo1; ?></th>
              <th>Puntos Periodo <?php echo $anio_periodo1; ?></th>
              <th>Puntos(%)</th>
              <th>Puntos Max. EFQM<?php echo $anio_periodo2; ?></th>
              <th>Puntos Periodo <?php echo $anio_periodo2; ?></th>
              <th>Puntos(%)</th>
            </tr>
          </thead>
          <tbody>
            <?php

            while ($row_comparacion_periodo = mysql_fetch_array($q_comparacion_periodos)) {
              $puntos_totales_efqm_obtenidos_periodo1 = $puntos_totales_efqm_obtenidos_periodo1 + $row_comparacion_periodo['puntos_en_periodo_1'];
              $puntos_totales_efqm_obtenidos_periodo2 = $puntos_totales_efqm_obtenidos_periodo2 + $row_comparacion_periodo['puntos_en_periodo_2'];
              $puntos_totales_efqm = $puntos_totales_efqm + $row_comparacion_periodo['ptos_efqm1'];

              ?>
              <tr>
                <td><?php echo $row_comparacion_periodo['criterio_nombre']; ?></td>
                <td style="text-align:right;"><?php echo $row_comparacion_periodo['ptos_efqm1']; ?></td>
                <td style="text-align:right;"><?php echo round($row_comparacion_periodo['puntos_en_periodo_1'], 2); ?></td>
                <td style="text-align:right;"><?php echo round(($row_comparacion_periodo['puntos_en_periodo_1'] *100 )/$row_comparacion_periodo['ptos_efqm1'], 2).'%'; ?></td>
                <td style="text-align:right;"><?php echo $row_comparacion_periodo['ptos_efqm2']; ?></td>
                <td style="text-align:right;"><?php echo round($row_comparacion_periodo['puntos_en_periodo_2'], 2); ?></td>
                <td style="text-align:right;"><?php echo round(($row_comparacion_periodo['puntos_en_periodo_2']*100)/$row_comparacion_periodo['ptos_efqm2'], 2).'%'; ?></td>
              </tr>
            <?php }
            if (mysql_num_rows($q_comparacion_periodos) > 0) {
              $porcentaje_efqm_periodo1 = $puntos_totales_efqm_obtenidos_periodo1 * 100 / $puntos_totales_efqm;
              $porcentaje_efqm_periodo2 = $puntos_totales_efqm_obtenidos_periodo2 * 100 / $puntos_totales_efqm;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="widget-box">
  
  <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>Informe Autoevaluacion Periodos: <?php echo $anio_periodo1; ?> - <?php echo $anio_periodo2; ?></h5>
  </div>
  <div class="widget-content nopadding">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Periodo</th>
          <th>Puntos EFQM periodo </th>
          <th>Porcentaje EFQM</th>
          <th>Sello EFQM</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <h2><?php echo $anio_periodo1 ?></h2>
          </td>
          <td>
            <h2><?php echo round($puntos_totales_efqm_obtenidos_periodo1, 2); ?></h2>
          </td>
          <td>
            <h2><?php echo round($porcentaje_efqm_periodo1, 2) . ' %'; ?></h2>
          </td>
          <td>
            <?php
            switch (true) {
              case $puntos_totales_efqm_obtenidos_periodo1 < 300: ?>
                <div align="center"><img src="img/compromiso.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;

              case $puntos_totales_efqm_obtenidos_periodo1 >= 300 && $puntos_totales_efqm_obtenidos_periodo1 < 400: ?>
                <div align="center"><img src="img/mas_300.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;

              case $puntos_totales_efqm_obtenidos_periodo1 >= 400 && $puntos_totales_efqm_obtenidos_periodo1 < 500: ?>
                <div align="center"><img src="img/mas_400.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;

              case $puntos_totales_efqm_obtenidos_periodo1 >= 500: ?>
                <div align="center"><img src="img/mas_500.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;
              default:
                break;
            }
            ?>
          </td>
        </tr>
        <tr>
          <td>
            <h2><?php echo $anio_periodo2 ?></h2>
          </td>
          <td>
            <h2><?php echo round($puntos_totales_efqm_obtenidos_periodo2, 2); ?></h2>
          </td>
          <td>
            <h2><?php echo round($porcentaje_efqm_periodo2, 2) . ' %'; ?></h2>
          </td>
          <td>
            <?php
            switch (true) {
              case $puntos_totales_efqm_obtenidos_periodo2 < 300: ?>
                <div align="center"><img src="img/compromiso.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;

              case $puntos_totales_efqm_obtenidos_periodo2 >= 300 && $puntos_totales_efqm_obtenidos_periodo2 < 400: ?>
                <div align="center"><img src="img/mas_300.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;

              case $puntos_totales_efqm_obtenidos_periodo2 >= 400 && $puntos_totales_efqm_obtenidos_periodo2 < 500: ?>
                <div align="center"><img src="img/mas_400.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;

              case $puntos_totales_efqm_obtenidos_periodo2 >= 500: ?>
                <div align="center"><img src="img/mas_500.png" width="20%" class="img-responsive" alt="Image"></div>
              <?php
                break;
              default:
                break;
            }
            ?>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>