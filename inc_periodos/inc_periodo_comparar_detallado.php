<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
                <h5>Seleccione para realizar una comparaci&oacute;n de periodos</h5>
            </div>

            <form action="comparacion_periodos_detallada.php" method="POST" role="form">
                <div style="width: 45%!important; display: inline-block;  margin-left: 30px;">
                    <select name="periodo1" id="input" class="form-control" required="required">
                        <option value="0">Seleccione Per&iacute;odo</option>
                        <option value="8">2010</option>
                        <option value="1">2012</option>
                        <option value="3">2015</option>
                        <option value="7">2016</option>
                        <option value="5">2017</option>
                        <option value="9">2019</option>
                    </select>
                </div>
                <div style="width: 45%!important; display: inline-block;  margin-left: 30px;">
                    <select name="periodo2" id="input" class="form-control" required="required">
                        <option value="0">Seleccione Per&iacute;odo</option>
                        <option value="8">2010</option>
                        <option value="1">2012</option>
                        <option value="3">2015</option>
                        <option value="7">2016</option>
                        <option value="5">2017</option>
                        <option value="9">2019</option>
                    </select>
                </div>
                <br>
                <br>
                <br>
                <button type="submit" class="btn btn-primary" style="margin-left: 30px;">Comparar</button>
            </form>
            <br>
        </div>
    </div>
</div>