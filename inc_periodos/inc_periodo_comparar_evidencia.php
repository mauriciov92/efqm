<style>
.flex-content{
    display: flex;
    justify-content: space-around;
    border-bottom: 6px solid #FFC9AE;
    background-color: #AEDFFF;
}
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>Ver Evidencias  <?php echo $anio_periodo1; ?> - <?php echo $anio_periodo2; ?></h5>
            </div>
            <div class="widget-content nopadding">
                
                <div style="width: 45%!important; display: inline-block;  margin-left: 30px;">
                    <select id="selectArea2" class="form-control" required="required">
                        <option value="0">Seleccione Area</option>
                        <?php
                        $consulta_areas =
                        "
                        SELECT  a.idarea, a.area_nombre
                        FROM area a
                        INNER JOIN  encuesta e ON a.idarea = e.area_idarea
                        INNER JOIN encuesta_calculo ec ON e.idencuesta = ec.encuesta_idencuesta
                        INNER JOIN
                            (SELECT a2.idarea, a2.area_nombre, ec2.periodo_idperiodo
                                FROM area a2
                                INNER JOIN encuesta e2 ON a2.idarea = e2.area_idarea
                                INNER JOIN encuesta_calculo ec2 ON e2.idencuesta = ec2.encuesta_idencuesta) s2 
                        ON s2.idarea = a.idarea
                        WHERE
                        ec.periodo_idperiodo = $idperiodo1
                        AND s2.periodo_idperiodo = $idperiodo2
                        GROUP BY a.idarea
                        ";
                        $q_consulta_areas=mysql_query($consulta_areas) or die(mysql_error());
                        while ($row_consulta_areas = mysql_fetch_array($q_consulta_areas)) {
                            echo '<option value="'.$row_consulta_areas['idarea'].'">'.$row_consulta_areas['area_nombre'].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div style="width: 45%!important; display: inline-block;  margin-left: 30px;">
                    <select  id="selectSubCriterio" class="form-control" required="required">
                        <option value="0">Seleccione Subcriterio</option>
                        <?php
                        $consulta_subcriterio = "
                            SELECT  sc.idsubcriterio AS id,
                            CONCAT(c.criterio_nombre,
                                    ' - ',
                                    sc.subcriterio_nombre,
                                    ' (',
                                    sc.descripcion,
                                    ')') AS descripcion
                            FROM
                            subcriterio sc
                            INNER JOIN
                            criterio c ON (sc.criterio_idcriterio = c.idcriterio)
                        ";
                        $q_consulta_subcriterio=mysql_query($consulta_subcriterio) or die(mysql_error());
                        while ($row_consulta_subcriterio = mysql_fetch_array($q_consulta_subcriterio)) {
                            echo '<option value="'.$row_consulta_subcriterio['id'].'">'.$row_consulta_subcriterio['descripcion'].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <br>
                <br>
                <button type="submit" id="verEvidencia" class="btn btn-primary" style="margin-left: 30px;">Ver Evidencias</button>
                <br>
                <br>
                <div class="widget-content nopadding" id="tablaEvidencia" hidden="hidden">

                <div class="flex-content">
                <div class="evidenciaperiodo1"></div>
                <div class="evidenciaperiodo2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {
    $(document).ready(function () {
        $("#verEvidencia").on('click',function () {
            var idperiodo1 = "<?php echo $idperiodo1; ?>";
            var idperiodo2 = "<?php echo $idperiodo2; ?>";
            var areaSeleccionada2 = $("#selectArea2").children("option:selected").val();
            var subCriterioSeleccionado = $("#selectSubCriterio").children("option:selected").val();
            var descripcionSubCriterio = $("#selectSubCriterio").children("option:selected").text();
            if(areaSeleccionada2 == "0" || subCriterioSeleccionado == "0"){
                alert ("Debe ingresar seleccionar alguna opcion de consulta");
                return false;
            }else{          
            $.ajax({
                type: "POST",
                url: "inc_periodos/inc_periodo_comparar_evidencia_ajax.php",
                data: {"idperiodo1":idperiodo1,"idperiodo2":idperiodo2,"areaSeleccionada":areaSeleccionada2,"subCriterioSeleccionado":subCriterioSeleccionado},
                dataType:"json"
            }).done(function(respuesta) {
                console.log(respuesta);
                if(respuesta.data && respuesta.data.length >=1){
                    var htmlevidenciaPeriodo1 = "<h4>Periodo "+<?php echo $anio_periodo1?>+"</h4><br><ul>";	
                    var htmlevidenciaPeriodo2 = "<h4>Periodo "+<?php echo $anio_periodo2?>+"</h4><br><ul>";	
                    var subcriterio="";
                    for(var i=0;i<respuesta.data.length;i++){
                        if(respuesta.data[i].idperiodo == idperiodo1){
                                htmlevidenciaPeriodo1 += "<li><a target='_blank' href= '"+respuesta.data[i].evidencia_url+"'>Ver Evidencia"+"&nbsp<i class='fa fa-eye' aria-hidden='true'></i></li>"
                        }else if (respuesta.data[i].idperiodo == idperiodo2) {
                            htmlevidenciaPeriodo2 += "<li><a target='_blank' href= '"+respuesta.data[i].evidencia_url+"'>Ver Evidencia"+"&nbsp<i class='fa fa-eye' aria-hidden='true'></i></li>"
                        }
						}
                        htmlevidenciaPeriodo1 += "</ul>";
                        htmlevidenciaPeriodo2 += "</ul>";
                        if(htmlevidenciaPeriodo1 === "<h4>Periodo "+<?php echo $anio_periodo1?>+"</h4><br><ul></ul>"){
                            $(".evidenciaperiodo1").empty().append("<h4>Periodo "+<?php echo $anio_periodo1?>+"</h4><p>No se cargaron evidencias en este subcriterio</p>");
                        }else{
                            $(".evidenciaperiodo1").empty().append(htmlevidenciaPeriodo1);
                        }
                        if(htmlevidenciaPeriodo2 === "<h4>Periodo "+<?php echo $anio_periodo2?>+"</h4><br><ul></ul>"){
                            $(".evidenciaperiodo2").empty().append("<h4>Periodo "+<?php echo $anio_periodo2?>+"</h4><p>No se cargaron evidencias en este subcriterio</p>");
                        }else{
                            $(".evidenciaperiodo2").empty().append(htmlevidenciaPeriodo2);
                        }
                        $("#tablaEvidencia").show();

                }else{
                        alert("No Fue posible realizar la comparación de los periodos");
                }

                }) ;
            }
            });

  

    });
        })(jQuery);
        
</script>