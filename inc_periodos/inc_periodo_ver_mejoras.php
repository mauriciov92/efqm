<?php $clase_colores=''; ?>
<div class="span6">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Areas Encuestadas Periodo: <?php echo $anio_periodo ?></h5>
    </div>
    <div class="widget-content">
      <table class="table table-bordered table-striped">
        <thead>
          <tr align="center" class="visible-print">
            <th colspan="3">Areas Encuestadas Periodo: <?php echo $anio_periodo ?></th>
          </tr>
          <tr>
            <th>Areas</th>
            <th>Fecha encuesta</th>
            <th>Estado encuesta</th>
            <th>Observaciones encuesta</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if (mysql_num_rows($q_areas_periodo) >0) {
          while ($row_areas_periodo=mysql_fetch_array($q_areas_periodo)) { ?>
            <tr>
              <td><?php echo $row_areas_periodo['area_nombre']; ?></td> 
              <td><?php echo $row_areas_periodo['fecha_encuenta']; ?></td>                
              <td><?php echo $row_areas_periodo['encuesta_estado']; ?></td>
              <td><?php echo $row_areas_periodo['encuesta_observacion']; ?></td>
          	</tr>  
          <?php } 
          }else{ ?>
          	<tr>
              <td colspan="4">
              	<div class="alert alert-info" align="center">
              		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              		<strong>No hay areas encuestdas</strong>
              	</div>
              </td> 
          	</tr>
          <?php } ?>            
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="span6">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Propuestas de Mejora Periodo: <?php echo $anio_periodo; ?></h5>
    </div>
    <div class="widget-content">
      <?php if ($tipo_persona!=4 && $tipo_persona!=3 && mysql_num_rows($q_mejora_periodo) >0) { ?>
        <div align="right" class="hidden-print">
          <a class="btn btn-link" href='mejoras_edicion.php?idperiodo=<?php echo $idperiodo;?>'>Cambiar estado de mejoras <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  
        </div>      
      <?php }?>
      <table class="table table-bordered table-striped">
        <thead>
          <tr align="center" class="visible-print">
            <th colspan="4">Propuestas de Mejora Periodo: <?php echo $anio_periodo; ?></th>
          </tr>
          <tr>
            <th>Mejora</th>
            <th>Area de mejora</th>
            <th>Prioridad</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if (mysql_num_rows($q_mejora_periodo) >0) {
          while ($row_q_mejora_periodo=mysql_fetch_array($q_mejora_periodo)) { 
            switch (true) {
              case strcmp($row_q_mejora_periodo['estado'], 'finalizado')==0:
                $clase_colores='class="success"';
                break;
              case strcmp($row_q_mejora_periodo['estado'], 'en proceso')==0:
                $clase_colores='class="info"';
                break;
              case strcmp($row_q_mejora_periodo['estado'], 'pendiente')==0:
                $clase_colores='class="warning"';
                break;
              default:                
                break;
            }
            ?>
            <tr <?php echo $clase_colores; ?>>
              <td><?php echo $row_q_mejora_periodo['mejoradescriocion']; ?></td> 
              <td><?php echo $row_q_mejora_periodo['area_nombre']; ?></td>                
              <td><?php echo $row_q_mejora_periodo['descripcion']; ?></td>
              <td><?php echo $row_q_mejora_periodo['estado']; ?></td>
            </tr>  
          <?php } 
          }else{ ?>
            <tr>
              <td colspan="4">
                <div class="alert alert-info" align="center">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>No hay propuestas de mejoras para este periodo</strong>
                </div>
              </td> 
            </tr>
          <?php } ?>
          <?php if ($tipo_persona!=4 && $tipo_persona!=3) { ?>
          <tr class="hidden-print">
            <td colspan="4">
              <div align="right">
                <a class="btn btn-link" href='mejoras_alta.php?idperiodo=<?php echo $idperiodo;?>'>Areas de Mejora <i class="fa fa-2x fa-plus-circle"></i></a>  
              </div>
            </td>
          </tr>   
          <?php }?>         
        </tbody>
      </table>
    </div>
  </div>
</div>