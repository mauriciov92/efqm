<div class="span6">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Criterios / Porcentaje sobre puntaje total</h5>
    </div>
    <div class="widget-content" align="center">
      <div id="container_pie" style="min-width: 100%; height: 100%; max-width: 600px; margin: 0 auto"></div>
    </div>
  </div>
</div>
<div class="span6">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Criterios / Puntos</h5>
    </div>
    <div class="widget-content" align="center">
      <div id="container_line" style="min-width: 100%; height: 100%; max-width: 600px; margin: 0 auto"></div>
    </div>
  </div>
</div>

<!------------------------------- GRAFICA DE LINEAL -------------------------------->
<script type="text/javascript">
  Highcharts.chart('container_line', {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Puntaje de criterios'
    },
    xAxis: {
        categories: [<?php echo $categorias_lineal; ?>]
    },
    yAxis: {
        title: {
            text: 'Puntos EFQM'
        },
        labels: {
            formatter: function () {
                return this.value+'%';
            }
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    },
    series: [{
        name: 'Puntos',
        marker: {
            symbol: 'square'
        },
        data: [<?php echo $data_lineal; ?>]

    }]
});
</script>
<!------------------------------- // FIN GRAFICA DE LINEAL -------------------------------->

<!------------------------------- GRAFICA DE TORTAS -------------------------------->
<script type="text/javascript">
  // Build the chart
Highcharts.chart('container_pie', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'Peso de Criterios'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Puntos criterio',
        colorByPoint: true,
        data: [<?php echo $data_grafica; ?>]
    }]
});
</script>
<!------------------------------- // FINGRAFICA DE TORTAS -------------------------------->