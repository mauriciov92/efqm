<?php 
  $periodos_existentes=array();
  while ($row_periodos=mysql_fetch_array($q_periodo)) {
    $periodos_existentes[]=$row_periodos['periodo_anio'];
  }
?>
<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Nuevo Periodo</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="periodo_alta_ok.php" method="POST" class="form-horizontal">
            <div class="control-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <label class="control-label">Selccione el periodo</label>
              <div class="controls">
                <select name="anio">
                  <?php 
                  for ($i=$inicio; $i <= $fin; $i++) {  
                    $existe=false;
                    for ($j=0; $j < count($periodos_existentes); $j++) { 
                      if ($periodos_existentes[$j]==$i) {
                        $existe=true;
                      }
                    }
                    if ($existe==false) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>  
                    <?php }
                  }?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Criterios</label>
              <div class="controls">
              	<?php include "inc_criterios/select_criterios.php"; ?>
              	<?php 
              		while ($row_criterios=mysql_fetch_array($q_criterios)) { ?>
						      <div class="control-group">
              				<label class="control-label"><?php echo $row_criterios['criterio_nombre']; ?> :</label>
              				<div class="controls">
              					<input type="hidden" name="idcriterio[]" class="form-control" value="<?php echo $row_criterios['idcriterio']; ?>" required="required" title="criterio">
                				<input type="number" value="" placeholder="Ingrese un valor" name="puntos_criterio[]" class="form-control calculo_input" value="" min="0" max="1000" required="required" title="Puntaje del periodo">
              				</div>
            			</div>              		
              		<?php } ?>					
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                  <div class="control-group">
                      <label class="control-label">Total puntos :</label>
                      <div class="controls">
                        <input type="number" class="form-control" min="0" max="1000" required="required" value="" title="Puntaje total de criterios" id="puntos_totales" readonly>
                      </div>
                  </div>                  
              </div>
            </div>
            <div class="alert alert-info">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <strong>La suma total de puntos de los criterios debe ser igual a 1000</strong>
                    </div>        
            <div class="form-actions" align="right">
              <button type="submit" id="guardar" class="btn btn-success" disabled>Guardar</button>
            </div>
          </form>
        </div>
      </div>      
    </div>
  </div>
  
  <script type="text/javascript">
    $(document).ready(function() {
      calcularTotal();
    });
    $('.calculo_input').on('change',function(event) {
      event.preventDefault();
      calcularTotal();
    });

    function calcularTotal(){
      var total=0;
      $('input[name="puntos_criterio[]"]').each(function() {
        if ($(this).val()!='') { 
          total=total+parseFloat($(this).val());
        }
      }); 
      $('#puntos_totales').prop('value', total);
      if (total==1000) {
        $('#guardar').removeProp('disabled');
      }
      
    }
  </script>
