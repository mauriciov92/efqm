<?php $clase_colores=''; ?>
<div class="span12">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Mejoras Sin Finalizar Periodo: <?php echo $anio_periodo_anterior; ?></h5>
    </div>
    <div class="widget-content">
      <table class="table table-bordered table-striped">
        <thead>
          <tr align="center" class="visible-print">
            <th colspan="4">Propuestas de mejora sin finalizar del periodo <?php echo $anio_periodo_anterior; ?></th>
          </tr>
          <tr>
            <th>Mejora</th>
            <th>Area de mejora</th>
            <th>Prioridad</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if (mysql_num_rows($q_mejora_periodo_anterior) >0) {
          while ($row_mejora_periodo_anterior=mysql_fetch_array($q_mejora_periodo_anterior)) { 
            switch (true) {
              case strcmp($row_mejora_periodo_anterior['estado'], 'en proceso')==0:
                $clase_colores='class="info"';
                break;
              case strcmp($row_mejora_periodo_anterior['estado'], 'pendiente')==0:
                $clase_colores='class="warning"';
                break;
              default:                
                break;
            }
            ?>
            <tr <?php echo $clase_colores; ?>>
              <td><?php echo $row_mejora_periodo_anterior['mejoradescriocion']; ?></td> 
              <td><?php echo $row_mejora_periodo_anterior['area_nombre']; ?></td>                
              <td><?php echo $row_mejora_periodo_anterior['descripcion']; ?></td>
              <td><?php echo $row_mejora_periodo_anterior['estado']; ?></td>
            </tr>  
          <?php } 
          }
          else{ ?>
            <tr>
              <td colspan="4">
                <div class="alert alert-info" align="center">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>No hay propuestas de mejoras sin finalizar en el periodo anterior</strong>
                </div>
              </td> 
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>