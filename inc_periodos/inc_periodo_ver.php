<?php
  $puntos_totales_efqm=0;
  $puntos_totales_efqm_obtenidos=0;
  $data_grafica="";
  $categorias_lineal="";
  $data_lineal="";
  $porcentaje_efqm=0;
?>
<div class="row-fluid">
<div class="span12">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Informe criterios periodo <?php echo $anio_periodo; ?></h5>
    </div>
    <div class="widget-content nopadding">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th class="hidden-print">Nro. Criterio</th>
            <th class="hidden-print">Tipo Criterio</th>
            <th>Criterio</th>
            <th>Puntos Max. EFQM</th>
            <th>Puntos EFQM <?php echo $organizacion_nombre; ?></th>
            <th>Porcentaje EFQM <?php echo $organizacion_nombre; ?></th>
          </tr>
        </thead>
        <tbody>
          <?php 
          while ($row_criterios_periodo=mysql_fetch_array($q_criterios_periodo)) { 
            $data_grafica.="{name: '$row_criterios_periodo[criterio_nombre]',y:$row_criterios_periodo[ptos_totales_obtenidos]},";
            $categorias_lineal.="'$row_criterios_periodo[criterio_nombre]',";
            $puntos_totales_efqm_obtenidos=$puntos_totales_efqm_obtenidos+$row_criterios_periodo['ptos_totales_obtenidos'];
            $puntos_totales_efqm=$puntos_totales_efqm+$row_criterios_periodo['ptos_efqm'];
            $porcentaje_criterio=$row_criterios_periodo['ptos_totales_obtenidos']*100/$row_criterios_periodo['ptos_efqm'];
            $puntos_porcentaje=round($porcentaje_criterio,2);
            $data_lineal.="$puntos_porcentaje,";
            ?>
            <tr>
              <td class="hidden-print"><?php echo $row_criterios_periodo['criterio_idcriterio']; ?></td> 
              <td class="hidden-print"><?php echo $row_criterios_periodo['criterio_tipo_descripcion']; ?></td>                
              <td><?php echo $row_criterios_periodo['criterio_nombre']; ?></td>
              <td><?php echo $row_criterios_periodo['ptos_efqm']; ?></td>
              <td><?php echo round($row_criterios_periodo['ptos_totales_obtenidos'],2); ?></td>
              <td><?php echo round($porcentaje_criterio,2).' %'; ?></td>
          </tr>  
          <?php } 
          if (mysql_num_rows($q_criterios_periodo)>0) {
            $porcentaje_efqm=$puntos_totales_efqm_obtenidos*100/$puntos_totales_efqm;
          }
          ?>            
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<div class="row-fluid">
<div class="span12">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Informe Autoevaluacion Periodo: <?php echo $anio_periodo; ?></h5>
    </div>
    <div class="widget-content nopadding">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>M&aacute;ximo EFQM</th>
            <th>Puntos EFQM periodo <?php echo $anio_periodo; ?></th>
            <th>Porcentaje EFQM</th>
            <th>Sello EFQM</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><h2><?php echo $puntos_totales_efqm; ?></h2></td>
            <td><h2><?php echo round($puntos_totales_efqm_obtenidos,2); ?></h2></td>
            <td><h2><?php echo round($porcentaje_efqm,2).' %'; ?></h2></td>
            <td>
              <?php 
              switch (true) {
                case $puntos_totales_efqm_obtenidos<300: ?>
                  <div align="center"><img src="img/compromiso.png" width="20%" class="img-responsive" alt="Image"></div>
                  <?php 
                  break;
                
                case $puntos_totales_efqm_obtenidos>=300 && $puntos_totales_efqm_obtenidos<400: ?>
                  <div align="center"><img src="img/mas_300.png" width="20%" class="img-responsive" alt="Image"></div>
                  <?php 
                  break;

                case $puntos_totales_efqm_obtenidos>=400 && $puntos_totales_efqm_obtenidos<500: ?>
                  <div align="center"><img src="img/mas_400.png" width="20%" class="img-responsive" alt="Image"></div>
                  <?php 
                  break;

                case $puntos_totales_efqm_obtenidos>=500: ?>
                  <div align="center"><img src="img/mas_500.png" width="20%" class="img-responsive" alt="Image"></div>
                  <?php 
                  break;
                default:
                  break;
              }
            ?>
          </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
