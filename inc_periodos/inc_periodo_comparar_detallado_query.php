<?php 
mysql_select_db($database_conexion_efqm, $conexion_efqm);

$categorias_lineal='';
$data_lineal=[];
$series=[];
$series_barra='';
$data_barra=0;
$idperiodo_array=array();
$idperiodo_array[]=$idperiodo1;
$idperiodo_array[]=$idperiodo2;
$arrayPeriodos=array();
$criterios=
"SELECT 
    criterio_nombre
FROM
    criterio";
$q_criterios=mysql_query($criterios) or die(mysql_error());

while ($row_criterios=mysql_fetch_array($q_criterios)) {
    $categorias_lineal.="'$row_criterios[criterio_nombre]',";
}
for ($i=0; $i < count($idperiodo_array); $i++) { 
    $criterios_periodos=
    "SELECT 
        periodo_anio,
        criterio_idcriterio,
        ptos_efqm,
        periodo_estado,
        ptos_totales_obtenidos,
        criterio_nombre,
        criterio_tipo_descripcion
    FROM
        periodo_has_criterio
            INNER JOIN
        criterio ON criterio_idcriterio = idcriterio
            INNER JOIN
        criterio_tipo ON criterio_tipo_idcriterio_tipo = idcriterio_tipo
            INNER JOIN
        periodo ON periodo_idperiodo = idperiodo
    WHERE
        periodo_idperiodo = $idperiodo_array[$i]";
        $data_lineal[$i]='';
    $q_criterios_periodos=mysql_query($criterios_periodos) or die(mysql_error());  
    while ($row_criterios_periodos=mysql_fetch_array($q_criterios_periodos)) { 
        $porcentaje_criterio=$row_criterios_periodos['ptos_totales_obtenidos']*100/$row_criterios_periodos['ptos_efqm'];
        $punto=round($porcentaje_criterio,2);
        $data_lineal[$i].="$punto,";        
        array_push($arrayPeriodos,array('periodo_anio'=> $row_criterios_periodos['periodo_anio'],'periodo_estado'=> $row_criterios_periodos['periodo_estado']));
        if ($i==0) {
            $anio_periodo1=$row_criterios_periodos['periodo_anio'];
        }
        else{
            $anio_periodo2=$row_criterios_periodos['periodo_anio'];
        }
    }    
}
$series[]=$anio_periodo1;
$series[]=$anio_periodo2;

$comparacion_periodos=
"SELECT
    c.criterio_nombre,
    p1.ptos_efqm as ptos_efqm1,
    p2.ptos_efqm as ptos_efqm2,
    p1.ptos_totales_obtenidos as puntos_en_periodo_1,
    p2.ptos_totales_obtenidos as puntos_en_periodo_2
FROM
    periodo_has_criterio p1
        INNER JOIN
    criterio c ON p1.criterio_idcriterio = c.idcriterio
    INNER JOIN 
        periodo_has_criterio p2 ON p2.criterio_idcriterio = c.idcriterio
        INNER JOIN
    criterio_tipo ct ON c.criterio_tipo_idcriterio_tipo = ct.idcriterio_tipo		 
WHERE
    p1.periodo_idperiodo = $idperiodo1
    AND 
    p2.periodo_idperiodo = $idperiodo2 
";
$q_comparacion_periodos=mysql_query($comparacion_periodos) or die(mysql_error());

?>