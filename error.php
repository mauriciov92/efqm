<?php require_once('conexion/conexion_efqm.php'); ?>

<!DOCTYPE html>
<html lang="en">
    
<head>
  <title>EFQM</title><meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="css/matrix-login.css" />
  <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
    
    <body style="background-color: white;">
      <div id="loginbox">            
        <form id="loginform" style="background-color: white;" class="form-vertical" action="sis_acceso_control.php" method="POST">
            <div align="center">
                <h2> La sesi&oacute;n no inicio correctamente. vuelva a intentarlo</h2>
            </div>
            <form>
                <a href="index.php" class="btn btn-primary pull-right">Volver</a>
                <div class="clearfix"></div>
            </form>
        </form>
      </div>
      <script src="js/jquery.min.js"></script>  
      <script src="js/matrix.login.js"></script> 

    </body>

</html>






