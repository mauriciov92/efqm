<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 

/* DEFINICION DE VARAIBLES */
  
  $idencuesta=$_POST['idencuesta'];
  $idperiodo=$_POST['idperiodo'];
  $encuesta_estado=$_POST['encuesta_estado'];
  $idencuesta_calculo=$_POST['idencuesta_calculo'];
  $resultado=$_POST['resultado'];
  $evidencia=$_POST['evidencia'];
  $observacion_calculo=$_POST['observacion_calculo'];
  $encuesta_observacion=$_POST['observacion'];

/* // FIN DEFINICION DE VARAIBLES */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_encuestas/inc_encuesta_header.php"; ?>
      
      <div class="container-fluid">        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: justify;">
          <?php include "inc_encuestas/inc_encuesta_editar_ok.php"; ?>
          <?php include "inc_encuestas/inc_encuesta_update_criterio.php" ?>
        </div>
      </div>
  </div>  
    <script type="text/javascript">
      $(document).ready(function() {
        $('#menu_principal').removeAttr('class');
        $('#menu_encuesta').attr('class', 'submenu active');
      });
    </script>
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>
