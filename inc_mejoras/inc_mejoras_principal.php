<?php 
$clase_fila_encuesta='';
?>
<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Mejoras</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Descripci&oacute;n</th>
                  <th>Prioridad</th>
                  <th>Area</th>
                  <th>Estado</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                  while ($row_periodo_mejoras=mysql_fetch_array($q_periodo_mejoras)) {
                
                    $mejoras=
                    "SELECT 
                        mejoradescriocion, estado, area_nombre, descripcion
                    FROM
                        mejora
                            INNER JOIN
                        encuesta ON encuesta_idencuesta = idencuesta
                            INNER JOIN
                        area ON area_idarea = idarea
                            INNER JOIN
                        prioridad_mejora ON prioridad_mejora_idprioridad_mejora = idprioridad_mejora
                    WHERE
                        periodo_idperiodo = $row_periodo_mejoras[idperiodo]";
                    $q_mejoras=mysql_query($mejoras) or die(mysql_error()); ?>
                    <tr class="periodo_titulo" onclick="mostrar_mejoras(<?php echo $row_periodo_mejoras['periodo_anio']; ?>);">
                      <td colspan="4">
                        <div>
                          <strong><?php echo $row_periodo_mejoras['periodo_anio'].' ( Cantidad Mejoras: '.mysql_num_rows($q_mejoras).')'; ?></strong>
                        </div>
                        <?php 
                        if (mysql_num_rows($q_mejoras)>0) { ?>
                          <div align="right"><a href="#"><i class="fa fa-caret-down" aria-hidden="true"></i></a></div>
                        <?php }
                        ?>
                      </td>
                    </tr>
                    <?php 
                    while ($row_mejoras=mysql_fetch_array($q_mejoras)) {
                      switch ($row_mejoras['estado']) {
                        case 'pendiente':
                          $clase_fila_encuesta='warning';
                          break;
                        
                        case 'en proceso':
                          $clase_fila_encuesta='info';
                          break;

                        case 'finalizado':
                          $clase_fila_encuesta='success';
                          break;

                        default:
                          break;
                      } ?>
                      <tr class="<?php echo $clase_fila_encuesta;?> <?php echo $row_periodo_mejoras['periodo_anio']; ?>" style="display: none;">
                        <td><a href="#"><?php echo $row_mejoras['mejoradescriocion']; ?></a></td>
                        <td><a href="#"><?php echo $row_mejoras['descripcion']; ?></a></td>
                        <td><a href="#"><?php echo $row_mejoras['area_nombre']; ?></a></td>
                        <td><a href="#"><?php echo $row_mejoras['estado']; ?></a></td>
                      </tr>
                    <?php } 
                  } ?>               
              </tbody>
            </table>
          </div>
          <div class="clearfix"></div>
          <br>
          <div><strong>*Referencias: </strong>
            <span class="label label-warning">Pendiente</span>
            <span class="label label-info">En proceso</span>
            <span class="label label-success">Finalizada</span>
          </div>
        </div>
      </div>      
    </div>
  </div>

<script type="text/javascript">
  function mostrar_mejoras(anio){
      $('.'+anio).toggle('slow');
  }
</script>  