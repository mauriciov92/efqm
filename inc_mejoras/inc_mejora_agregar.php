<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Nueva Mejora para periodo <?php echo $anio; ?></h5>
        </div>
        <div class="widget-content nopadding">
          <form action="mejoras_alta_ok.php" method="POST" class="form-horizontal">
            <input type="hidden" name="periodo_idperiodo" class="form-control" value="<?php echo $idperiodo; ?>">
            <input type="hidden" name="estado" class="form-control" value="1">
            <div class="control-group">
              <label class="control-label">Seleccione Area</label>
              <div class="controls">
                <select name="encuesta_idencuesta">
                  <?php 
                  while ($row_areas_mejora=mysql_fetch_array($q_areas_mejora)) { ?>
                    <option value="<?php echo $row_areas_mejora['idencuesta']; ?>"><?php echo $row_areas_mejora['area_nombre']; ?></option>
                  <?php }?>
                </select>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Prioridad</label>
              <div class="controls">
                <select name="idprioridad_mejora">
                  <?php 
                  while ($row_prioridad_mejora=mysql_fetch_array($q_prioridad_mejora)) { ?>
                    <option value="<?php echo $row_prioridad_mejora['idprioridad_mejora']; ?>"><?php echo $row_prioridad_mejora['descripcion']; ?></option>
                  <?php }?>
                </select>	
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Descripci&oacute;n :</label>
              <div class="controls">
                <textarea name="mejoradescripcion" class="form-control" rows="3" cols="10" required="required" placeholder="Ingrese descripci&oacute;n de la mejora"></textarea>
              </div>
            </div>        
            <div class="form-actions" align="right">
              <button type="submit" id="guardar" class="btn btn-success">Guardar</button>
            </div>
          </form>
        </div>
      </div>      
    </div>
  </div>
