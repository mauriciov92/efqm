<div class="span12">
  <div class="widget-box">
    <div class="widget-title hidden-print"> <span class="icon"> <i class="icon-signal"></i> </span>
      <h5>Mejoras Periodo: <?php echo $anio_periodo; ?></h5>
    </div>
    <div class="widget-content">
      <form action="mejoras_edicion_ok.php" method="POST">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Mejora</th>
            <th>Area de mejora</th>
            <th>Prioridad</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if (mysql_num_rows($q_mejora_periodo) >0) { ?>
            <input type="hidden" name="idperiodo" id="input" class="form-control" value="<?php echo $idperiodo ?>">
          <?php 
          while ($row_mejora_periodo=mysql_fetch_array($q_mejora_periodo)) { ?>
            <tr>
              <td><?php echo $row_mejora_periodo['mejoradescriocion']; ?></td> 
              <td><?php echo $row_mejora_periodo['area_nombre']; ?></td>                
              <td><?php echo $row_mejora_periodo['descripcion']; ?></td>
              <td>
                <input type="hidden" name="idmejora[]" class="form-control" value="<?php echo $row_mejora_periodo['idmejora']; ?>">
                <select name="estado[]">
                  <option value="1" <?php if (strcmp($row_mejora_periodo['estado'], 'pendiente')==0) { echo "selected";} ?>>Pendiente</option>
                  <option value="2" <?php if (strcmp($row_mejora_periodo['estado'], 'en proceso')==0) { echo "selected";} ?>>En Proceso</option>
                  <option value="3" <?php if (strcmp($row_mejora_periodo['estado'], 'finalizado')==0) { echo "selected";} ?>>Finalizado</option>
                </select>
              </td>
            </tr>  
          <?php } ?>
          <tr>
            <td colspan="4">
              <div class="form-actions" align="right">
                <button type="submit" id="actualizar" class="btn btn-success">Actualizar</button>
              </div>
            </td>
          </tr>
          <?php }
          else{ ?>
            <tr>
              <td colspan="4">
                <div class="alert alert-info" align="center">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>No hay propuestas de mejoras sin finalizar en el periodo anterior</strong>
                </div>
              </td> 
            </tr>
          <?php } ?>
        </tbody>
      </table>
      </form>
    </div>
  </div>
</div>