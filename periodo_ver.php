<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 

/* DEFINICION DE VARIABLES*/

  $idperiodo=$_GET['idperiodo'];

/*// FIN DEFINICION DE VARIABLES*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_periodos/inc_periodo_header.php"; ?>
      
      <div class="container-fluid">     
        <?php include "inc_periodos/inc_periodo_ver_query.php" ?>
        <div class="row-fluid visible-print" align="center">
          <div style="font-size: 22px;">
            <strong>Universidad Tecnol&oacute;gica Nacional - Facultad Reginal Tucum&aacute;n (UTN - FRT)</strong>
          </div>
          <br>
          <div style="font-size: 22px;">
            <strong>Informe EFQM periodo <?php echo $anio_periodo; ?></strong>
          </div>
          <br>
          <div>
            <legend></legend>
          </div>
        </div>

        <div class="row-fluid hidden-print" align="right">
          <div class="span12">
            <a href="#" id="imprimir_informe" title="Imprimir Informe"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a>
          </div>
        </div>

        <?php include "inc_periodos/inc_periodo_ver.php" ?>
        <div class="row-fluid">
          <?php include "inc_periodos/inc_periodo_ver_grafica.php" ?>
        </div>
        
        <div class="row-fluid">
          <?php include "inc_periodos/inc_periodo_ver_mejoras_anterior_query.php" ?>
          <?php include "inc_periodos/inc_periodo_ver_mejoras_anterior.php" ?>
        </div>        

        <div class="row-fluid">
          <?php include "inc_periodos/inc_periodo_areas_query.php" ?>
          <?php include "inc_periodos/inc_periodo_ver_mejoras_query.php" ?>
          <?php include "inc_periodos/inc_periodo_ver_mejoras.php" ?>
        </div>
        
        <div class="row-fluid">
          <?php include "inc_periodos/inc_periodo_ver_evidencias_query.php" ?>
          <?php include "inc_periodos/inc_periodo_ver_evidencias.php" ?>
        </div>
        <?php 
        if ($_SESSION['tipo_persona']==1 && $estado_periodo!='finalizado') { ?>
          <div class="row-fluid hidden-print" align="right">
            <form action="finalizar_periodo.php" method="POST" role="form">
              <input type="hidden" name="idperiodo" id="inputIdperiodo" class="form-control" value="<?php echo $idperiodo; ?>">
              <button type="submit" class="btn btn-danger">Finalizar Periodo</button>
            </form>
          </div>
        <?php } ?>
      </div>
    </div>

    <script type="text/javascript">
      
      $('#imprimir_informe').on('click', function(event) {
        //printdiv('container_pie');
        window.print();
      });
      /*
      function printdiv(printdivname)
        {
        var headstr = "<html><head><title>Booking Details</title></head><body>";
        var footstr = "</body>";
        var newstr = document.getElementById(printdivname).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
        }
        */
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
      $('#menu_principal').removeAttr('class');
      $('#menu_periodo').attr('class', 'submenu active');
    });
    </script>
    
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>