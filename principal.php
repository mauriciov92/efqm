<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include "sis_header.php"; ?>
</head>

<body>

  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_principal/inc_principal_header.php"; ?>

    <div class="container-fluid">

        <?php include "sis_btn_acciones.php"; ?>
        <?php include "inc_periodos/select_periodos.php" ?>
        <?php include "inc_principal/inc_principal_query.php" ?>
        <div class="row-fluid">
          <?php include "inc_principal/inc_principal_grafica_totales.php" ?>
        </div>
        <div class="row-fluid">
          <?php include "inc_principal/inc_principal_grafica.php" ?>
        </div>
      </div>
  </div>  
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>

</html>