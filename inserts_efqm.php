<?php 
/* CRITERIOS********************************************************************************************/
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (1,'Liderazgo',1);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (2,'Politica y Estrategia',1);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (3,'Personas',1);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (4,'Alianzas y Recursos',1);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (5,'Procesos',1);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (7,'Resultados en las Personas',2);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (6,'Resultados en los Clientes',2);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (8,'Resultados en la Sociedad',2);
INSERT INTO `criterio` (`idcriterio`,`criterio_nombre`,`criterio_tipo_idcriterio_tipo`) VALUES (9,'Resultados Clave',2);

/* SUBCRITERIOS********************************************************************************************/


INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (1,'1a',1);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (2,'1b',1);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (3,'1c',1);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (4,'1d',1);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (5,'1e',1);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (6,'2a',2);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (7,'2b',2);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (8,'2c',2);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (9,'2d',2);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (10,'3a',3);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (11,'3b',3);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (12,'3c',3);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (13,'3d',3);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (14,'3e',3);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (15,'4a',4);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (16,'4b',4);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (17,'4c',4);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (18,'4d',4);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (19,'4e',4);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (20,'5a',5);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (21,'5b',5);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (22,'5c',5);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (23,'5d',5);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (24,'5e',5);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (25,'6a',6);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (26,'6b',6);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (27,'7a',6);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (28,'7b',6);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (29,'8a',8);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (30,'8b',8);

INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (31,'9a',9);
INSERT INTO `subcriterio` (`idsubcriterio`,`subcriterio_nombre`,`criterio_idcriterio`) VALUES (32,'9b',9);

/* PREGUNTAS********************************************************************************************/

INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (12,'¿Han sido definidos por parte del Equipo Directivo la Misión, la Visión, los Principios Éticos y Valores que conforman la \"Cultura\" de la Universidad, y han sido convenientemente divulgados a todos los Grupos de Interés (profesores, alumnos, Personal de Administración y Servicios (PAS), Consejo Social, etc.)?',1);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (13,'¿Incluyen dichos valores conceptos tales como calidad y aprendizaje? ',1);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (14,'El comportamiento del Equipo Directivo ¿es coherente con dichos valores?',1);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (15,'¿Impulsa la Gerencia el desarrollo e implantación de un Sistema de Gestión por Procesos que permita traducir la Estrategia/Presupuesto a objetivos cuantificables que, medidos de forma periódica, sirvan para mejorar los resultados globales?',2);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (16,'¿Se relaciona el Equipo Directivo con los profesores, alumnos, socios y representantes de la sociedad para conocer sus necesidades y expectativas?',3);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (17,'¿Se implican y apoyan actividades de mejora, protección medioambiental o de contribución a la sociedad?',3);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (18,'¿Mantiene el Equipo Directivo una comunicación fluida y bidireccional con sus empleados?',4);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (19,'¿Aprovechan dicha comunicación para transmitir los Valores y las Estrategias de la Universidad? ',4);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (20,'¿Escuchan las aportaciones y/o quejas de sus empleados?',4);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (21,'¿Apoya el Equipo Directivo a sus empleados y les ayudan si es necesario para conseguir sus objetivos? ',4);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (22,'¿Les animan y facilitan la participación en equipos/actividades de mejora? ',4);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (23,'¿Reconocen adecuadamente a individuos o equipos por su contribución a dichas actividades?',4);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (24,'¿Define e impulsa el Equipo Directivo los cambios necesarios para adecuar la Universidad? ',5);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (25,'¿Garantizan la inversión, los recursos y el apoyo necesarios para desarrollar dichos cambios? ',5);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (26,'¿Una vez producidos los cambios, se mide la eficacia de los mismos y se comunican a los Grupos de Interés?',5);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (27,'En la definición del Plan Operativo/Presupuesto Anual de la Unidad Organizativa, ¿se recogen y consideran las Necesidades y Expectativas de los diferentes Grupos de Interés (empleados de la propia Unidad, estudiantes, potenciales estudiantes, PDI, PAS, centros, departamentos, órganos de gobierno, Admones. Públicas e instituciones conveniadas), así como datos sobre el comportamiento del sistema universitario y unidades similares?',6);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (28,'En la definición del Plan Operativo/Presupuesto Anual de la Unidad Organizativa, ¿se recogen y consideran los resultados de las mediciones anteriores, tanto propios como de unidades similares, y se analizan los impactos de la legislación aplicable, la innovación tecnológica, y los indicadores socioeconómicos y demográficos a corto y largo plazo?',7);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (29,'En la definición del Plan Operativo/Presupuesto Anual de la Unidad Organizativa, ¿se tienen en cuenta: Coherencia con los Principios Éticos y Valores que conforman su \"Cultura\"?',8);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (30,'En la definición del Plan Operativo/Presupuesto Anual de la Unidad Organizativa, ¿se tienen en cuenta: Atención equilibrada de necesidades y expectativas de los Grupos de Interés.?',8);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (31,'En la definición del Plan Operativo/Presupuesto Anual de la Unidad Organizativa, ¿se tienen en cuenta: Análisis de Riesgos y Plazos e identificación de los Factores Críticos de Éxito?',8);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (32,'¿Se identifican los Procesos Clave de la Unidad Organizativa y se despliega el Plan Operativo a través de ellos? ',9);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (33,'¿Se descomponen los Objetivos y Metas de la Unidad Organizativa a través de los distintos niveles, llegando hasta la definición y seguimiento de los objetivos individuales o de equipo?',9);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (34,'¿Se evalúa el nivel de conocimiento y sensibilización de los Grupos de Interés sobre los aspectos fundamentales de la Estrategia, Plan Operativo y Presupuesto, según sea apropiado?',9);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (35,'¿Existe un Plan específico de Personal de Administración y Servicios, respetuoso con la legislación vigente y la igualdad de oportunidades, alineado con la Estrategia y el Plan Operativo de la Universidad, y se revisa conjuntamente con ellos? ',10);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (36,'¿Se tienen en cuenta las opiniones del Personal de Administración y Servicios en la definición de dicho Plan?',10);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (37,'¿Se preocupa el Equipo Directivo del desarrollo personal y profesional del Personal de Administración y Servicios, procurando la adecuación de sus conocimientos y experiencia a las necesidades derivadas de su responsabilidad? ',11);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (38,'¿Se desarrollan e implantan planes de formación que faciliten dicha adecuación? ',11);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (39,'¿Se asigna al Personal de Administración y Servicios objetivos individuales y de equipo, y se evalúa su rendimiento?',11);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (40,'¿Estimula el Equipo Directivo la implicación del PAS hacia la consecución de sus objetivos, mediante la motivación y el reconocimiento? ',12);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (41,'¿Promueve y facilita el Equipo Directivo la participación del PAS en acciones de mejora?',12);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (42,'¿Impulsa y motiva al PAS hacia la innovación y la creatividad, siendo receptiva a sus aportaciones y sugerencias de mejora?',12);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (43,'¿Se preocupa el Equipo Directivo de establecer una buena comunicación con/entre sus empleados?',13);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (44,'¿Se han desarrollado cauces de comunicación verticales y horizontales y se utilizan eficientemente? ',13);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (45,'¿Se aprovechan dichos canales de comunicación para difundir el conocimiento y las buenas prácticas?',13);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (46,'¿Se asegura el Equipo directivo del alineamiento de sus políticas de remuneración, movilidad, etc., con el Plan Operativo y Presupuesto? ',14);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (47,'¿Existe una política de reconocimiento hacia el PAS y fomento de la concienciación en temas medioambientales y de seguridad e higiene? ',14);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (48,'¿Se proporciona al PAS unas instalaciones y servicios de alta calidad? ',14);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (49,'¿Existe sensibilidad ante necesidades personales/clientes?',14);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (50,'¿Identifica la Unidad Organizativa aquellas Organizaciones clave con las que se relaciona (Universidades, administraciones públicas, proveedores, etc.)?',15);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (51,'¿Desarrolla con ellas acuerdos de colaboración, fomentando la transferencia de conocimientos y el aprovechamiento de sinergias?',15);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (52,'¿Se ha definido e implantado una estrategia económico-financiera, alineada con la Estrategia/Plan Operativo de la Unidad Organizativa y traducida en un Presupuesto Anual? ',16);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (53,'¿Contempla dicha estrategia tanto las inversiones previstas, como los recursos necesarios para la financiación de las actividades de la Unidad Organizativa?',16);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (54,'¿Contempla una adecuada gestión del riesgo financiero, gestión de cobros (cuando proceda), etc.?',16);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (55,'¿Se asegura la Unidad Organizativa del adecuado funcionamiento, conservación y seguridad de sus edificios e instalaciones? ',17);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (56,'¿Se optimizan recursos, inventarios y se reducen consumos de suministros y energías (principalmente, los no renovables)? ',17);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (57,'¿Se cuidan adecuadamente los aspectos medioambientales y de reciclado de residuos?',17);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (58,'¿Identifica la Unidad Organizativa las tecnologías e instalaciones más adecuadas para cubrir sus necesidades y las de sus clientes? ',18);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (59,'¿Gestiona adecuadamente las tecnologías existentes y se preocupa de su actualización y renovación?',18);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (60,'¿Recoge y gestiona adecuadamente la Unidad Organizativa toda la información pertinente para el cumplimiento de sus fines? ',19);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (61,'¿Facilita a sus Grupos de Interés el acceso a las informaciones que son de su interés?',19);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (62,'¿Protege adecuadamente la información sensible, tanto para la gestión como para las personas?',19);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (63,'¿La Facultad dispone y aplica una metodología de procesos orientada a la identificación, diseño y documentación de sus Procesos Clave, que son aquéllos considerados imprescindibles para desplegar y desarrollar la Estrategia y Plan Operativo? ',20);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (64,'¿Dicha metodología de procesos se corresponde con alguna estandarización del tipo ISO 9000 propia de la Universidad o similar?',20);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (65,'¿Disponen los Procesos, y en especial los denominados Clave, de unos sistemas de medición o indicadores, que permitan establecer sus objetivos de rendimiento y evaluar los resultados obtenidos? ',20);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (66,'¿Se han identificado aquellas áreas de los procesos que son comunes con otras unidades y a agentes externos a éste (proveedores, administración, etc.)?',20);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (67,'¿Se revisa regularmente la eficiencia de los Procesos y se modifican apropiadamente en función de dichas revisiones, así como en función de las informaciones procedentes de sugerencias de mejora, actividades de aprendizaje, propuestas de innovación, etc.? ',21);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (68,'La implantación de los cambios en los Procesos, ¿se realiza mediante un análisis previo (piloto) y una adecuada comunicación/formación a todos los implicados?',21);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (69,'¿Se recogen informaciones procedentes de estudios de mercado y competencia, necesidades y expectativas de clientes, sugerencias innovadoras y creativas..., ?',22);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (70,'¿se tiene en cuenta esta informacion a la hora de definir los nuevos Productos, Servicios y actividades de la Unidad organizativa?',22);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (71,'¿Se investigan las necesidades y expectativas, así como el grado de satisfacción de los clientes con los Productos y Servicios, y se utiliza dicha información para la modificación y mejora de los mismos?',22);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (72,'¿Se asegura la Unidad organizativa de que las características y prestaciones de los Productos y Servicios que proporciona a sus clientes responden a las especificaciones de su diseño? ',23);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (73,'¿Comunica veraz y adecuadamente la Unidad organizativa las condiciones de prestación de sus Productos y Servicios a sus potenciales clientes? ',23);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (74,'¿Establece niveles de compromiso y es consecuente con los mismos?',23);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (75,'¿Desarrolla la Unidad organizativa actividades encaminadas a identificar necesidades y expectativas de sus clientes? ',24);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (76,'¿Dispone la Unidad organizativa de cauces de comunicación para la recepción de quejas y reclamaciones de sus clientes? ',24);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (77,'¿Tramita las mismas de forma sistemática y utiliza dicha información para la mejora permanente de sus servicios?',24);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (78,'¿Identifica la Universidad cuáles son los aspectos más significativos y que más aprecian sus clientes? ',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (79,'¿El método que se utiliza para identificar dichos aspectos es fiable, se revisa de forma periódica y permite segmentar los resultados en función de los diferentes grupos de clientes?',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (80,'¿Obtiene periódicamente los Servicios de la Universidad información directa del grado de satisfacción de los diferentes grupos de clientes respecto a dichos aspectos más significativos, así como de los servicios recibidos, y el nivel de satisfacción global?',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (81,'Sobre los indicadores del grado de satisfacción de clientes, ¿se marcan objetivos y se miden los resultados obtenidos? ',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (82,'¿La tendencia de dichos indicadores es positiva? ',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (83,'Si en alguno no lo fuera, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (84,'¿Se comparan los índices de satisfacción de clientes con los de los Servicios en otras Universidades? ',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (85,'Respecto a dichos indicadores, ¿en qué situación competitiva situamos a la Universidad?',25);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (86,'Teniendo en cuenta cuáles son los aspectos más valorados por los clientes, ¿ha identificado la Universidad con qué procesos están relacionados y con qué indicadores de dichos procesos puede existir una correspondencia?',26);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (87,'Sobre los indicadores de dichos procesos, que inciden directamente en la satisfacción de los clientes, ¿se marcan objetivos y se miden los resultados obtenidos? ',26);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (88,'¿La tendencia de dichos indicadores es positiva? ',26);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (89,'Si en alguno no lo fuera, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',26);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (90,'¿Se comparan los resultados de dichos indicadores con los de otras Universidades o el propio sector? ',26);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (91,'Respecto a dichos indicadores, ¿en qué situación competitiva situamos a los Servicios de la Universidad?',26);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (92,'¿Obtiene periódicamente la Facultad información directa del grado de satisfacción de los diferentes grupos de empleados respecto a aquellos aspectos que les son más significativos, así como del nivel de satisfacción global?',27);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (93,'Sobre los indicadores del grado de satisfacción de los empleados, ¿se marcan objetivos y se miden los resultados obtenidos? ',27);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (94,'¿La tendencia de dichos indicadores es positiva? ',27);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (95,'Si en alguno no lo fuera, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',27);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (96,'¿Se comparan los índices de satisfacción de los empleados con los de otras Unidades o Universidades? ',27);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (97,'Respecto a dichos indicadores, ¿en qué situación competitiva situamos a la Unidad Organizativa?',27);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (98,'¿Ha identificado la Facultad con qué procesos están relacionados los índices de satisfacción de los empleados, y con qué indicadores de dichos procesos puede existir una correspondencia?',28);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (99,'Sobre los indicadores de dichos procesos, que inciden directamente en la satisfacción de los empleados, ¿se marcan objetivos y se miden los resultados obtenidos? ',28);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (100,'¿La tendencia de dichos indicadores es positiva? ',28);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (101,'Si en alguno no lo fuera, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',28);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (102,'¿Se comparan los resultados de dichos indicadores con los de otras unidades de la propia Universidad y/o Universidades? ',28);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (103,'Respecto a dichos indicadores, ¿en qué situación situamos a la Unidad Organizativa?',28);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (104,'¿Identifica y mide la Unidad Organizativa el nivel de percepción que tiene la Sociedad respecto a aquellos aspectos de especial sensibilidad social en su esfera de influencia? ',29);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (105,'¿La tendencia de dichos indicadores es positiva? ',29);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (106,'Si en alguno no lo fuera, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',29);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (107,'¿Ha identificado la Unidad Organizativa con qué procesos están relacionados los índices de percepción social, y con qué indicadores de dichos procesos puede existir una correspondencia? ',30);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (108,'¿Se marcan objetivos sobre dichos indicadores y se miden los resultados obtenidos? ',30);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (109,'¿La tendencia de dichos indicadores es positiva? ',30);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (110,'Si en alguno no lo fuera, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',30);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (111,'¿Se comparan los índices de percepción social de la Unidad Organizativa con los de otras Unidades de la propia Universidad o con Unidades Organizativas similares y de Universidades líderes? ',30);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (112,'Respecto a dichos indicadores, ¿en qué situación competitiva situamos a la Unidad Organizativa?',30);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (113,'¿Define objetivos y mide la Unidad Organizativa de forma periódica y sistemática sus Resultados Clave, y especialmente los económico-financieros?',31);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (114,'¿Los objetivos son cada vez más exigentes y los resultados muestran una tendencia positiva? ',31);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (115,'Si alguno de los Resultados Clave no reflejara una tendencia positiva, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',31);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (116,'¿Se comparan los Resultados Clave con los de otras unidades organizativas o  universidades? ',31);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (117,'Respecto a dichos indicadores de Resultados Clave, ¿en qué situación competitiva situamos a la Unidad Organizativa?',31);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (118,'Además de dichos Resultados Clave, ¿define objetivos y mide la Facultadde forma periódica y sistemática otros resultados correspondientes a procesos de soporte, que contribuyen de manera sustancial a la consecución de los anteriores?',32);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (119,'Los objetivos de dichos indicadores correspondientes a procesos de soporte, ¿son cada vez más exigentes y los resultados muestran una tendencia positiva?',32);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (120,'Si alguno de los resultados no reflejara una tendencia positiva, ¿se han averiguado las causas y establecido las acciones de mejora adecuadas?',32);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (121,'¿Se comparan los resultados de los indicadores de procesos de soporte con los de otras Unidades Organizativas o Universidades? ',32);
INSERT INTO `pregunta` (`idpregunta`,`pregunta_descripcion`,`subcriterio_idsubcriterio`) VALUES (122,'Respecto a dichos indicadores, ¿en qué situación competitiva situamos a la Unidad Organizativa?',32);

/* PERIODOS********************************************************************************************/
INSERT INTO `periodo` (`idperiodo`,`periodo_anio`,`periodo_descripcion`) VALUES (1,'2012','');
INSERT INTO `periodo` (`idperiodo`,`periodo_anio`,`periodo_descripcion`) VALUES (3,'2015','');
INSERT INTO `periodo` (`idperiodo`,`periodo_anio`,`periodo_descripcion`) VALUES (7,'2016','');

/* PERIODO_CRITERIO********************************************************************************************/

INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (1,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (2,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (3,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (4,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (5,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (6,1,150.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (7,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (8,1,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (9,1,150.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (1,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (2,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (3,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (4,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (5,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (6,3,150.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (7,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (8,3,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (9,3,150.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (1,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (2,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (3,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (4,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (5,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (6,7,150.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (7,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (8,7,100.00);
INSERT INTO `periodo_has_criterio` (`criterio_idcriterio`,`periodo_idperiodo`,`ptos_efqm`) VALUES (9,7,150.00);

/* ORGANIZACION********************************************************************************************/
INSERT INTO `organizacion` (`idorganizacion`, `organizacion_nombre`) VALUES ('1', 'UTN - FRT');

/* AREAS ********************************************************************************************/
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('1', 'Decano', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('2', 'Secretaría Academica', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('3', 'Secretaría de Planeamiento', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('4', 'Secretaría Administrativa', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('5', 'Secretaría de Vinculación Tecnológica', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('6', 'Secretaría de Ciencia y Tecnología', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('7', 'Secretaría de Extensión Universitaria', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('8', 'Secretaría de Asuntos Estudiantiles', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('9', 'Secretaria de Cultura y Comun icación Institucional', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('10', 'Dirección de Tecnologías de Información y Comunicación', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('11', 'Dirección de Graduados', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('12', 'Dirección de Escuela de Posgrado', '1');
INSERT INTO `area` (`idarea`, `area_nombre`, `organizacion_idorganizacion`) VALUES ('13', 'Coordinador de Relaciones Institucionales', '1');

/* TIPO PERSONA ********************************************************************************************/

INSERT INTO `persona_tipo` (`idpersona_tipo`, `persona_tipo_descripcion`) VALUES ('1', 'Entrevistador');
INSERT INTO `persona_tipo` (`idpersona_tipo`, `persona_tipo_descripcion`) VALUES ('2', 'Entrevistado');

/* PERSONA ********************************************************************************************/

INSERT INTO `persona` (`personanombre`, `persona_tipo_idpersona_tipo`) VALUES ('ND-Entrevistador', '1');
INSERT INTO `persona` (`personanombre`, `persona_tipo_idpersona_tipo`) VALUES ('ND-Entrevistado', '2');

/* USUARIO ********************************************************************************************/

INSERT INTO `usuario` (`usuario_nombre`, `usuario_pass`, `persona_idpersona`, `usuario_estado`) VALUES ('Nombre_Usuario', MD5('1234'), '2', '1');
?>
