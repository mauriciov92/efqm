<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 

/* DEFINICION DE VARAIBLES */
  $idperiodo=$_POST['idperiodo'];
  $estado=$_POST['estado'];
  $idmejoras=$_POST['idmejora'];
/* // FIN DEFINICION DE VARAIBLES */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Mejoras</a></div>
    </div>
      
      <div class="container-fluid">        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: justify;">
          <?php include "inc_mejoras/inc_mejoras_edicion_ok.php" ?>
        </div>
      </div>
  </div>  
    <script type="text/javascript">
      $(document).ready(function() {
        $('#menu_principal').removeAttr('class');
        $('#menu_areas_mejora').attr('class', 'submenu active');
      });
    </script>
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>