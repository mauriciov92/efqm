<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php

/* DEFINICION DE VARIABLES*/

$idperiodo1 = $_POST['periodo1'];
$idperiodo2 = $_POST['periodo2'];


/*// FIN DEFINICION DE VARIABLES*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include "sis_header.php"; ?>
</head>

<body>

  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_periodos/inc_periodo_header.php"; ?>
    <div class="container-fluid">
      <?php include "inc_periodos/inc_periodo_comparar_detallado_query.php"; ?>
      <div class="row-fluid visible-print" align="center">
          <div style="font-size: 22px;">
            <strong>Universidad Tecnol&oacute;gica Nacional - Facultad Reginal Tucum&aacute;n (UTN - FRT)</strong>
          </div>
          <br>
          <div style="font-size: 22px;">
            <strong>Informe EFQM periodo <?php echo $anio_periodo; ?></strong>
          </div>
          <br>
          <div>
            <legend></legend>
          </div>
        </div>
        <div class="row-fluid hidden-print" align="right">
          <div class="span12">
            <a href="#" id="imprimir_informe" title="Imprimir Informe"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a>
          </div>
        </div>
      <?php include "inc_periodos/inc_periodo_comparar_detallado_ver.php"; ?>
      <div class="row-fluid">
      <?php include "inc_principal/inc_principal_grafica.php" ?>
      </div>
      <?php include "inc_periodos/inc_periodo_comparar_preguntas_por_areaycriterio.php"?>
      <?php include "inc_periodos/inc_periodo_comparar_evidencia.php"?>
      <?php include "inc_periodos/inc_periodo_comparar_areas_de_mejora.php"?>
    </div>
  </div>
  <script type="text/javascript">

  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#menu_principal').removeAttr('class');
      $('#menu_periodo').attr('class', 'submenu active');
      $('#imprimir_informe').on('click', function(event) {
        //printdiv('container_pie');
        window.print();
      });
    });
  </script>

  <?php include "sis_script.php"; ?>
</body>

</html>