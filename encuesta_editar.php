<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 

/* DEFINICION DE VARIABLES*/

  $idencuesta=$_GET['idencuesta'];

/*// FIN DEFINICION DE VARIABLES*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_encuestas/inc_encuesta_header.php"; ?>
      
      <div class="container-fluid">
        <?php// include "sis_btn_acciones.php"; ?>
        <div class="row-fluid">
          <div class="span12">
            <?php include "inc_encuestas/inc_encuesta_editar_query.php" ?>
            <?php include "inc_encuestas/inc_encuesta_editar.php" ?>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#menu_principal').removeAttr('class');
        $('#menu_encuesta').attr('class', 'submenu active');
      });
    </script>

  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>