<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> EFQM - Informaci&oacute;n</a></div>
    </div>
      
      <div class="container-fluid">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center">
          <img src="img/efqm-calidad.png" class="img-responsive" alt="Image">
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: justify;">
          <h5>
          Cuando hablamos de EFQM solemos referirnos al modelo de calidad definido por la fundación que lleva dicho nombre. Precisamente es la Fundación Europea para la Gestión de la Calidad, una fundación sin ánimo de lucro y con sede en Bruselas que cuenta con más de 500 socios repartidos en más de 55 países.
          Esta fundación define el modelo EFQM de Calidad y Excelencia como vía para la autoevaluación y la determinación de los procesos de mejora continua en entornos empresariales tanto privados como públicos.
          Los principales conceptos que conforman el modelo EFQM son:
          <ul>
            <li>Orientación hacia los resultados</li>
            <li>Orientación al cliente</li>
            <li>Liderazgo y coherencia</li>
            <li>Gestión por procesos y hechos</li>
            <li>Desarrollo e implicación de las personas</li>
            <li>Proceso continuo de aprendizaje, innovación y mejora</li>
            <li>Desarrollo de alianzas</li>
            <li>Responsabilidad social de la organización</li>
          </ul>
          A través de la autoevaluación el modelo EFQM pretende una gestión más eficaz y eficiente. La identificación de los puntos fuertes y débiles aplicados a diferentes ámbitos de la organización son el punto de partida para el proceso de mejora continua.
          El Club de Excelencia en la gestión otorga unos reconocimientos en forma de sellos que valora la implementación EFQM en una empresa según la puntuación obtenida.          
          </h5>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center">
          <img src="img/sellos-efqm.png" class="img-responsive" alt="Image">
        </div>
        <hr/>
      </div>
  </div>  
  <script type="text/javascript">
    $(document).ready(function() {
      $('#menu_principal').removeAttr('class');
      $('#menu_info_efqm').attr('class', 'active');
    });
  </script>
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>