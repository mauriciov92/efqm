<?php 
  $clase_fila_encuesta='';
?>
<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Encuestas</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Fecha alta</th>
                  <th>Periodo</th>
                  <th>Area</th>
                  <th>Estado</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                  while ($row_periodo_encuesta=mysql_fetch_array($q_periodo_encuesta)) {
                
                    $encuestas=
                    "SELECT 
                        idencuesta,
                        fecha_encuenta,
                        encuesta_estado,
                        area_nombre,
                        periodo_anio
                    FROM
                        encuesta
                            INNER JOIN
                        area ON area_idarea = idarea
                            INNER JOIN
                        encuesta_calculo ON idencuesta = encuesta_idencuesta
                            INNER JOIN
                        periodo ON periodo_idperiodo = idperiodo
                    WHERE idperiodo=$row_periodo_encuesta[idperiodo]
                    GROUP BY idencuesta
                    ORDER BY idencuesta DESC";
                    $q_encuesta=mysql_query($encuestas) or die(mysql_error()); ?>
                    <tr class="periodo_titulo" onclick="mostrar_encuestas(<?php echo $row_periodo_encuesta['periodo_anio']; ?>);">
                      <td colspan="4">
                        <div>
                          <strong><?php echo $row_periodo_encuesta['periodo_anio'].' ( Cantidad Encuestas: '.mysql_num_rows($q_encuesta).')'; ?></strong>
                        </div>
                        <?php 
                        if (mysql_num_rows($q_encuesta)>0) { ?>
                          <div align="right"><a href="#"><i class="fa fa-caret-down" aria-hidden="true"></i></a></div>
                        <?php }
                        ?>
                      </td>
                    </tr>
                    <?php 
                    while ($row_encuesta=mysql_fetch_array($q_encuesta)) {
                      switch ($row_encuesta['encuesta_estado']) {
                        case 'Pendiente':
                          $clase_fila_encuesta='warning';
                          break;
                        
                        case 'En proceso':
                          $clase_fila_encuesta='info';
                          break;

                        case 'Finalizada':
                          $clase_fila_encuesta='success';
                          break;

                        default:
                          break;
                      } ?>
                      <tr class="<?php echo $clase_fila_encuesta;?> <?php echo $row_periodo_encuesta['periodo_anio']; ?>" style="display: none;">
                        <td><a href="encuesta_editar.php?idencuesta=<?php echo $row_encuesta['idencuesta']; ?>"><?php echo $row_encuesta['fecha_encuenta']; ?></a></td>
                        <td><a href="encuesta_editar.php?idencuesta=<?php echo $row_encuesta['idencuesta']; ?>"><?php echo $row_encuesta['periodo_anio']; ?></a></td>
                        <td><a href="encuesta_editar.php?idencuesta=<?php echo $row_encuesta['idencuesta']; ?>"><?php echo $row_encuesta['area_nombre']; ?></a></td>
                        <td><a href="encuesta_editar.php?idencuesta=<?php echo $row_encuesta['idencuesta']; ?>"><?php echo $row_encuesta['encuesta_estado']; ?></a></td>
                      </tr>
                    <?php } 
                  } ?>               
              </tbody>
            </table>
          </div>
          <div class="clearfix"></div>
          <br>
          <div><strong>*Referencias: </strong>
            <span class="label label-warning">Pendiente</span>
            <span class="label label-info">En proceso</span>
            <span class="label label-success">Finalizada</span>
          </div>
        </div>
      </div>      
    </div>
  </div>

<script type="text/javascript">
  function mostrar_encuestas(anio){
      $('.'+anio).toggle('slow');
  }
</script>  