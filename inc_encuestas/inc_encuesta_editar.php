<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>Preguntas - Resultados</h5>
  </div>
  <div class="widget-content nopadding">
    <div align="center"><h2><?php echo $area_nombre; ?></h2></div>
    <form action="encuesta_editar_ok.php" method="POST" id="formulario_envio" class="form-horizontal">
      <input type="hidden" name="idencuesta" id="idencuesta" class="form-control" value="<?php echo $idencuesta; ?>">
      <input type="hidden" name="encuesta_estado" id="encuesta_estado" class="form-control" value="2">
    <?php 
    while ($row_criterios=mysql_fetch_array($q_criterios)) {
      /* PREGUNTAS DE ENCUESTA CALCULO PARA MOSTRAR SEPARADAS POR CRITERIOS */
      $preguntas=
      "SELECT 
        idencuesta_calculo,
        periodo_idperiodo,
        pregunta_descripcion,
        resultado,
        evidencia,
        encuesta_calculo.observacion AS observacion_calculo
      FROM
          encuesta_calculo
              INNER JOIN
          pregunta ON pregunta_idpregunta = idpregunta
              INNER JOIN
          subcriterio ON subcriterio_idsubcriterio = idsubcriterio
      WHERE
          encuesta_idencuesta = $idencuesta
              AND criterio_idcriterio = $row_criterios[idcriterio]";
        $q_preguntas=mysql_query($preguntas) or die(mysql_error());
        ?>
  
        <div align="left" style="margin-left:1%;" class="criterio_titulo" onclick="mostrar_preguntas(<?php echo $row_criterios['idcriterio']; ?>);">
          <h4><?php echo $row_criterios['criterio_nombre']; ?><button type="button" class="btn btn-link"><i class="fa fa-caret-down" aria-hidden="true"></i></button></h4>
        </div>
        <legend></legend>
        <div style="display: none;" id="<?php echo $row_criterios['idcriterio']; ?>">
        <?php 
        while ($row_preguntas_criterio=mysql_fetch_array($q_preguntas)) { ?>
          <div class="control-group">
            <input type="hidden" name="idencuesta_calculo[]" id="idencuesta_calculo" class="form-control" value="<?php echo $row_preguntas_criterio['idencuesta_calculo']; ?>">
            <input type="hidden" name="idperiodo" id="idperiodo" class="form-control" value="<?php echo $row_preguntas_criterio['periodo_idperiodo']; ?>">
            <div class="span8">
              <?php echo $row_preguntas_criterio['pregunta_descripcion']; ?>
              <div class="clearfix"></div>                  
            </div>
            <div class="controls">              
              <div class="span2">
                Resultado
                <select name="resultado[]" <?php if (strcmp($estado_encuesta, 'Finalizada')==0 || $tipo_persona==4 || $tipo_persona==3) { echo "disabled"; } ?> >
                  <option value="1" <?php if ($row_preguntas_criterio['resultado']==0) {echo "selected";} ?>>Nada (0%)</option>
                  <option value="2" <?php if ($row_preguntas_criterio['resultado']==25) {echo "selected";} ?>>Poco (25%)</option>
                  <option value="3" <?php if ($row_preguntas_criterio['resultado']==50) {echo "selected";} ?>>Bastante (50%)</option>
                  <option value="4" <?php if ($row_preguntas_criterio['resultado']==75) {echo "selected";} ?>>Mucho (75%)</option>
                  <option value="5" <?php if ($row_preguntas_criterio['resultado']==100) {echo "selected";} ?>>Totalmente (100%)</option>
                </select>  
              </div>
              <div class="span2">
                Evidencia
                <select name="evidencia[]" <?php if (strcmp($estado_encuesta, 'Finalizada')==0 || $tipo_persona==4 || $tipo_persona==3) { echo "disabled"; } ?> >
                  <option value="1" <?php if ($row_preguntas_criterio['evidencia']==0) {echo "selected";} ?>>Nada (0%)</option>
                  <option value="2" <?php if ($row_preguntas_criterio['evidencia']==25) {echo "selected";} ?>>Poco (25%)</option>
                  <option value="3" <?php if ($row_preguntas_criterio['evidencia']==50) {echo "selected";} ?>>Bastante (50%)</option>
                  <option value="4" <?php if ($row_preguntas_criterio['evidencia']==75) {echo "selected";} ?>>Mucho (75%)</option>
                  <option value="5" <?php if ($row_preguntas_criterio['evidencia']==100) {echo "selected";} ?>>Totalmente (100%)</option>
                </select>  
              </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="span11">
              <textarea id="" class="form-control" maxlength="500" name="observacion_calculo[]" style="width: 100%!important;" placeholder="Inserte respuesta del entrevistado (Longitud m&aacute;xima 500 caracteres) "><?php echo $row_preguntas_criterio['observacion_calculo'] ?></textarea>                
            </div>
            <div class="clearfix"></div>
          </div>
        <?php }?>
        </div>
        <br>
        <br>
    <?php } ?>
      <div class="control-group">
        <label class="control-label"><strong>Observaci&oacute;n :</strong></label>
        <div class="controls">
          <input type="text" name="observacion" class="span11" value="<?php echo $observacion_encuesta; ?>" placeholder="Oberservaciones" <?php if (strcmp($estado_encuesta, 'Finalizada')==0 || $tipo_persona==4 || $tipo_persona==3) { echo "readonly"; } ?> />
        </div>
      </div>
    <?php 
    if (strcmp($estado_encuesta, 'Finalizada')!=0 && ($tipo_persona!=4 && $tipo_persona!=3)) { ?>
      <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <div>
          <strong>* Finalizar: guarda los cambios y cambia el estado de la encuesta a "Finalizada"</strong>
        </div>
        <div>
          <strong>* Guardar: guarda los cambios y cambia el estado de la encuesta a "En Proceso".</strong>
        </div>
      </div>
      <div class="form-actions" align="right">
        <button type="button" id="btn_finalizar" class="btn btn-primary">Finalizar Encuesta</button>
        <button type="submit" class="btn btn-success">Guardar Cambios</button>
      </div>
    <?php } ?>
    </form>
  </div>
</div>      
    
<script type="text/javascript">
  $('#btn_finalizar').on('click', function(event) {
    event.preventDefault();
    $('#encuesta_estado').val(3);
    $( "#formulario_envio" ).submit();
  });
</script>
<script type="text/javascript">
  function mostrar_preguntas(idcriterio){
      $('#'+idcriterio).toggle('slow');
  }
</script>