<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Nueva Encuesta</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="encuesta_alta_preguntas.php" method="POST" class="form-horizontal">
            <div align="right">
              <input type="date" name="fecha_encuesta" id="inputFecha_encuesta" class="form-control" value="<?php echo $fecha?>" readonly>
            </div>
            <div class="control-group">
              <label class="control-label">Seleccione Periodo</label>
              <div class="controls">
                <?php include "inc_periodos/select_periodos_pendientes.php"; ?>
                <select name="idperiodo">
                <?php 
                while ($row_periodos=mysql_fetch_array($q_periodo)) { ?>
                  <option value="<?php echo $row_periodos['idperiodo']; ?>"><?php echo $row_periodos['periodo_anio']; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Seleccione Area</label>
              <div class="controls">
                <?php include "inc_areas/select_area.php"; ?>
                <select name="idarea">
                <?php 
                while ($row_areas=mysql_fetch_array($q_areas)) { ?>
                  <option value="<?php echo $row_areas['idarea']; ?>"><?php echo $row_areas['area_nombre']; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Seleccione Entrevistador/es</label>
              <div class="controls">
                <?php include "inc_personas/select_personas_entrevistador.php"; ?>
                <select name="identrevistador">
                <?php 
                while ($row_entrevistador=mysql_fetch_array($q_personas)) { ?>
                  <option value="<?php echo $row_entrevistador['idpersona']; ?>"><?php echo $row_entrevistador['personanombre']; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Seleccione Responsable</label>
              <div class="controls">
                <?php include "inc_personas/select_personas_entrevistado.php"; ?>
                <select name="identrevistado">
                <?php 
                while ($row_entrevistado=mysql_fetch_array($q_personas_entrevistado)) { ?>
                  <option value="<?php echo $row_entrevistado['idpersona']; ?>"><?php echo $row_entrevistado['personanombre']; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>  
            <div class="form-actions" align="right">
              <button type="submit" class="btn btn-success">Siguente</button>
            </div>
          </form>
        </div>
      </div>      
    </div>
  </div>
