<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>

<?php 

/* DEFINICIOIN DE VARIABLES */
$idperiodo = $_POST['idperiodo'];
$idsubriterio=$_POST['idsubriterio'];
$idarea=$_POST['idarea'];
$descripcion=$_POST['descripcion'];
$subcriterio="";
$area="";

/*// FIN DEFINICIOIN DE VARIABLES */

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_periodos/inc_periodo_header.php"; ?>
      
      <div class="container-fluid">  
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: justify;">
          <?php include "inc_evidencias/inc_evidencia_alta_ok.php" ?>
        </div>
      </div>
  </div>  

  <script type="text/javascript">
    $(document).ready(function() {
      $('#menu_principal').removeAttr('class');
      $('#menu_periodo').attr('class', 'submenu active');
    });
  </script>

  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>
