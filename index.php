<!DOCTYPE html>
<html lang="en">
    
<head>
  <title>EFQM</title><meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="css/matrix-login.css" />
  <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
    
    <body style="background-color: white;">
      <div id="loginbox">            
        <form id="loginform" style="background-color: white;" class="form-vertical" action="sis_acceso_control.php" method="POST">
          <div class="control-group normal_text" style="background-color: white;"> <h3><img src="img/efqm.png" width="30%" alt="Logo" /></h3></div>
          <div class="control-group">
            <div class="controls">
              <div class="main_input_box">
                <span class="add-on bg_lg"><i class="icon-user"> </i></span><input name="usuario_nombre" type="text" placeholder="Nombre de usuario" />
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <div class="main_input_box">
                <span class="add-on bg_ly"><i class="icon-lock"></i></span><input name="usuario_pass" type="password" placeholder="Contraseña" />
              </div>
            </div>
          </div>
          <div align="right" class="form-actions">
            <button type="submit" class="btn btn-success">Iniciar Sesi&oacute;n</button>
          </div>
        </form>
      </div>
      <script src="js/jquery.min.js"></script>  
      <script src="js/matrix.login.js"></script> 

    </body>

</html>
