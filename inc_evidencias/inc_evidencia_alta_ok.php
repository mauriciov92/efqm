<?php 
mysql_select_db($database_conexion_efqm, $conexion_efqm);

$cantidad_insertada=0;
/******************* INSERT EVIDENCIA *******************/

if (isset($_FILES["archivo"]["tmp_name"]) and $_FILES["archivo"]["tmp_name"][0] != "" ) {
  for ($i=0; $i < count($_FILES["archivo"]["tmp_name"]) ; $i++) { 
    $archivo_origen=$_FILES['archivo']['tmp_name'][$i];
    $carpeta="evidencias/doc/idperiodo_".$idperiodo."/";
    if (!is_dir($carpeta)) {
    mkdir($carpeta);
    }
    $nombre_archivo=$_FILES['archivo']['name'][$i];
    $archivo_destino=$carpeta.$nombre_archivo;
    move_uploaded_file($archivo_origen, $archivo_destino);
    $alta_evidencia="INSERT INTO evidencia (evidencia_url,evidencia_descripcion) VALUES ('$archivo_destino','$descripcion')";
    mysql_query($alta_evidencia) or die(mysql_error());
    if (mysql_affected_rows()>0) {
      $cantidad_insertada++;
    }
    $select_ultima_evidencia=
    "SELECT 
        MAX(idevidencia) AS idevidencia
    FROM
        evidencia";
    $q_select_ultima_evidencia=mysql_query($select_ultima_evidencia) or die(mysql_error());
    $row_select_ultima_evidencia=mysql_fetch_array($q_select_ultima_evidencia);
    $ultima_evidencia = $row_select_ultima_evidencia['idevidencia']; 
    for ($j=0; $j < count($idsubriterio) ; $j++) {
      $subcriterio = $idsubriterio[$j];
      for ($k=0; $k < count($idarea) ; $k++) { 
        $area=$idarea[$k];
        $alta_subcriterio_has_evidencia = "INSERT INTO subcriterio_has_evidencia(subcriterio_idsubcriterio,evidencia_idevidencia,periodo_idperiodo,area_idarea) VALUES ('$subcriterio','$ultima_evidencia','$idperiodo','$area')";
      mysql_query($alta_subcriterio_has_evidencia) or die(mysql_error());
      if (mysql_affected_rows()>0) {
      $cantidad_insertada++;
      }
      }
    }
    
  }
  }


/******************* // FIN INSERT EVIDENCIA *******************/

if ($cantidad_insertada > 0) { ?>
	<div class="alert alert-success" align="center">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Evidencia Agregada con &eacute;xito</strong>
		<br>
		<a href="evidencia_alta.php?idperiodo=<?php echo $idperiodo ?>"><button type="button" class="btn btn-default">Cargar otra</button></a>
    <a href="periodo_ver.php?idperiodo=<?php echo $idperiodo ?>"><button type="button" class="btn btn-default">Volver</button></a>
	</div>
<?php }
else{ ?>
	<div class="alert alert-danger" align="center">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Error al intentar agregar la mejora.</strong>
		<br>
		<a href="evidencia_alta.php?idperiodo=<?php echo $idperiodo ?>"><button type="button" class="btn btn-default">Cargar nuevamente</button></a>
    <a href="periodo_ver.php?idperiodo=<?php echo $idperiodo ?>"><button type="button" class="btn btn-default">Volver</button></a>
	</div>
<?php } ?>