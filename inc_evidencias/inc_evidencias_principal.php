<?php 
$clase_fila_encuesta='';
?>
<div class="row-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Evidencias</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Evidencia</th>
                  <th>Descripci&oacute;n</th>
                  <th>Area Relaciondas</th>
                  <th>Subcriterios relacionados</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                  while ($row_periodo_evidencias=mysql_fetch_array($q_periodo_evidencias)) {
                
                    $evidencias=
                    "SELECT 
                         idevidencia, evidencia_url, evidencia_descripcion
                     FROM
                         evidencia
                             INNER JOIN
                         subcriterio_has_evidencia ON idevidencia = evidencia_idevidencia
                     WHERE
                         periodo_idperiodo = $row_periodo_evidencias[idperiodo]
                     GROUP BY idevidencia";
                    $q_evidencia = mysql_query($evidencias) or die(mysql_error()); ?>
                    <tr class="periodo_titulo" onclick="mostrar_mejoras(<?php echo $row_periodo_evidencias['periodo_anio']; ?>);">
                      <td colspan="4">
                        <div>
                          <strong><?php echo $row_periodo_evidencias['periodo_anio']; ?></strong>
                        </div>
                        <?php 
                        if (mysql_num_rows($q_evidencia)>0) { ?>
                          <div align="right"><a href="#"><i class="fa fa-caret-down" aria-hidden="true"></i></a></div>
                        <?php }
                        ?>
                      </td>
                    </tr>
                    <?php 
                    while ($row_evidencias=mysql_fetch_array($q_evidencia)) {
                        $area_evidencia=
                         "SELECT 
                             area_nombre
                         FROM
                             subcriterio_has_evidencia
                                 INNER JOIN
                             area ON area_idarea = idarea
                         WHERE
                             periodo_idperiodo = $row_periodo_evidencias[idperiodo]
                                 AND evidencia_idevidencia = $row_evidencias[idevidencia]
                         GROUP BY idarea";
                         $q_area_evidencia=mysql_query($area_evidencia) or die(mysql_error());
                         $subcriterio_evidencia=
                         "SELECT DISTINCT(subcriterio_nombre) 
                         FROM
                             subcriterio_has_evidencia
                                 INNER JOIN
                             subcriterio ON subcriterio_idsubcriterio = idsubcriterio
                         WHERE
                             periodo_idperiodo = $row_periodo_evidencias[idperiodo]
                                 AND evidencia_idevidencia = $row_evidencias[idevidencia]";
                         $q_subcriterio_evidencia=mysql_query($subcriterio_evidencia) or die(mysql_error());
                       ?>
                      <tr class="<?php echo $clase_fila_encuesta;?> <?php echo $row_periodo_evidencias['periodo_anio']; ?>" style="display: none;">
                        <td><a href="<?php echo $row_evidencias['evidencia_url']; ?>" download><?php echo $row_evidencias['evidencia_url']; ?></a> </td>
                        <td><?php echo $row_evidencias['evidencia_descripcion']; ?></td>
                        <td>
                          <?php   
                            while ($row_area_evidencia=mysql_fetch_array($q_area_evidencia)) {
                              echo $row_area_evidencia['area_nombre'].', ';
                            }
                          ?>
                        </td>
                        <td>
                          <?php 
                            while ($row_subcriterio_evidencia=mysql_fetch_array($q_subcriterio_evidencia)) {
                              echo $row_subcriterio_evidencia['subcriterio_nombre'].', ';
                            }
                          ?>
                        </td>
                      </tr>
                    <?php } 
                  } ?>               
              </tbody>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>      
    </div>
  </div>

<script type="text/javascript">
  function mostrar_mejoras(anio){
      $('.'+anio).toggle('slow');
  }
</script>  