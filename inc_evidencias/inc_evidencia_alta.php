  <div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>Form Elements</h5>
  </div>
  <div class="widget-content nopadding">
    <form action="evidencia_alta_ok.php" method="POST" class="form-horizontal" enctype="multipart/form-data">   
      <input type="hidden" name="idperiodo" value="<?php echo $idperiodo; ?>">
     <div class="widget-content nopadding">
       <div class="control-group">
         <label class="control-label">Seleccione los subcriterios</label>
         <div class="controls">
           <select name="idsubriterio[]" multiple='multiple' >
             <?php 
               while ($row_subcriterios=mysql_fetch_array($q_subcriterios)) { ?>
                <option value="<?php echo $row_subcriterios['idsubcriterio']; ?>"><?php echo $row_subcriterios['subcriterio_nombre']." del criterio ".$row_subcriterios['criterio_nombre']; ?></option>
             <?php } ?>
           </select>
         </div>
       </div>
       <div class="control-group">
         <label class="control-label">Seleccione las areas</label>
         <div class="controls">
           <select name="idarea[]" multiple='multiple' >
             <?php 
               while ($row_area=mysql_fetch_array($q_areas)) { ?>
                 <option value="<?php echo $row_area['idarea']; ?>"><?php echo $row_area['area_nombre']; ?></option>
             <?php } ?>
           </select>
         </div>
       </div>
       <div class="control-group " >
         <label class="control-label">Descripcion/Observacion</label>
         <div class="controls">
           <textarea name="descripcion"></textarea> 
       </div>
       <div class="control-group">
         <label class="control-label">Archivos</label>
         <div class="controls">
           <input type="file" id='archivo[]'' name="archivo[]" multiple />
         </div>
       </div>
     </div>
     </div>
     <div class="form-actions" align="right">
      <button type="submit" id="guardar" class="btn btn-success">Guardar</button>
     </div>
    </form>
  </div>
</div>