<?php require_once('conexion/conexion_efqm.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
  $fecha=date('Y-m-d');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "sis_header.php"; ?>
</head>
<body>
  
  <?php include "sis_menu_usuario.php"; ?>
  <?php include "sis_menu_principal.php"; ?>

  <div id="content">
    <?php include "inc_encuestas/inc_encuesta_header.php"; ?>
      
      <div class="container-fluid">
        <?php// include "sis_btn_acciones.php"; ?>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: justify;">
          <?php include "inc_pruebas/inc_prueba_query.php" ?>
          <?php include "inc_pruebas/inc_prueba_grid.php" ?>
        </div>
      </div>
  </div>  
  <?php include "sis_footer.php"; ?>
  <?php include "sis_script.php"; ?>
</body>
</html>